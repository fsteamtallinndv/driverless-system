import math
import random
import matplotlib.pyplot as plt


def pythagoras(a, b):
    length = math.sqrt(a ** 2 + b ** 2)
    return length


def supplier_of_trajectory(yellow, best_trajectory):
    random_length = random.randint(0, len(yellow) - 1)
    random_start = random.randint(1, len(yellow) - 1)
    trajectory = []
    random_pos = random.uniform(-0.5, 0.5)
    if best_trajectory:
        if random_start + random_length < len(yellow):
            trajectory.extend(best_trajectory[:random_start])
            for pos in best_trajectory[random_start: random_start + random_length + 1]:
                trajectory.append(min(+1, max(-1, round(pos + random_pos, 4))))
            trajectory.extend(best_trajectory[random_start + random_length + 1:])
        else:
            for pos2 in best_trajectory[random_start:] + best_trajectory[
                                                         :random_length - (len(yellow) - random_start - 1)]:
                trajectory.append(min(+1, max(-1, round(pos2 + random_pos, 4))))
            trajectory.extend(best_trajectory[random_length - (len(yellow) - random_start - 1):random_start])
    else:
        for points in range(len(yellow)):
            trajectory.append(round(random.uniform(-1, 1), 4))
    return trajectory


def position_to_coordinate(yellow, blue, trajectory):
    """Calculates the given position ( -1 < position < 1) into coordinates.

    A and B are the cones on each side of track.
    x = A(x) + (B(x) - A(x)) * (1 - position)/2
    y = A(y) + (B(y) - A(y)) * (1 - position)/2
    :param yellow: list of cones on the right side
    :param blue: list og cones on the left side
    :param trajectory: list of positions the track needs to go through
    """
    pos_coordinates = []
    for i in range(len(trajectory)):
        x = yellow[i][0] + (blue[i][0] - yellow[i][0]) * (1 - (trajectory[i] * -1)) / 2
        y = yellow[i][1] + (blue[i][1] - yellow[i][1]) * (1 - (trajectory[i] * -1)) / 2
        pos_coordinates.append((x, y))
    return pos_coordinates


def grade_based_on_time(yellow, blue, trajectory):
    acceleration_max = 1.2
    deceleration_max = -0.7
    max_velocity = 115 / 3.6
    velocity_constant = 0.5 * 9.81
    max_v_constant = velocity_constant / (max_velocity ** 2)
    time = 0
    pos_coordinates = position_to_coordinate(yellow, blue, trajectory)
    speed_list = []
    distance_list = []
    for index in range(len(pos_coordinates)):
        if index < (len(pos_coordinates) - 2):
            vector_one = (pos_coordinates[index][0] - pos_coordinates[index + 1][0],
                          pos_coordinates[index][1] - pos_coordinates[index + 1][1])
            vector_two = (pos_coordinates[index + 2][0] - pos_coordinates[index + 1][0],
                          pos_coordinates[index + 2][1] - pos_coordinates[index + 1][1])

            length_vector_one = pythagoras(vector_one[0], vector_one[1])
            length_vector_two = pythagoras(vector_two[0], vector_two[1])
            distance = length_vector_one + length_vector_two
            distance_list.append(distance)
            cos_between_vectors = (vector_one[0] * vector_two[0] + vector_one[1] * vector_two[1]) / (
                    length_vector_two * length_vector_one)
            if cos_between_vectors > 1:
                cos_between_vectors = 1
            elif cos_between_vectors < -1:
                cos_between_vectors = -1
            beeta = math.acos(cos_between_vectors)
            if beeta > math.pi:
                beeta = 2 * math.pi - beeta
            alfa = math.pi - beeta
            if alfa > max_v_constant * distance:
                radius = distance / alfa
                speed = math.sqrt(velocity_constant * radius)
            else:
                speed = max_velocity
            speed_list.append(speed)

        elif index > (len(pos_coordinates) - 2):
            for index in range(-2, 0):
                vector_one = (pos_coordinates[index][0] - pos_coordinates[index + 1][0],
                              pos_coordinates[index][1] - pos_coordinates[index + 1][1])
                vector_two = (pos_coordinates[index + 2][0] - pos_coordinates[index + 1][0],
                              pos_coordinates[index + 2][1] - pos_coordinates[index + 1][1])

                length_vector_one = pythagoras(vector_one[0], vector_one[1])
                length_vector_two = pythagoras(vector_two[0], vector_two[1])
                distance = length_vector_one + length_vector_two
                distance_list.append(distance)
                cos_between_vectors = (vector_one[0] * vector_two[0] + vector_one[1] * vector_two[1]) / (
                        length_vector_two * length_vector_one)
                if cos_between_vectors > 1:
                    cos_between_vectors = 1
                elif cos_between_vectors < -1:
                    cos_between_vectors = -1
                beeta = math.acos(cos_between_vectors)
                if beeta > math.pi:
                    beeta = 2 * math.pi - beeta
                alfa = math.pi - beeta
                if alfa > max_v_constant * distance:
                    radius = distance / alfa
                    speed = math.sqrt(velocity_constant * radius)
                else:
                    speed = max_velocity
                speed_list.append(speed)
    # print(' '.join(['%.2f' % (s,) for s in speed_list]), 'first')
    for i in range(len(speed_list)):
        delta_time_acc = (-speed_list[i - 1] + math.sqrt(
            speed_list[i - 1] ** 2 + 2 * acceleration_max * distance_list[i])) / acceleration_max
        delta_velocity_acceleration = acceleration_max * delta_time_acc
        if speed_list[i] > delta_velocity_acceleration + speed_list[i - 1]:
            speed_list[i] = delta_velocity_acceleration + speed_list[i - 1]
    for i in range(len(speed_list) - 1, -1, -1):
        velocity_one = math.sqrt(speed_list[i] ** 2 - 2 * deceleration_max * distance_list[i])
        if speed_list[i - 1] > velocity_one:
            speed_list[i - 1] = velocity_one
    for t in range(len(speed_list)):
        time += distance_list[t] / speed_list[t]
    return time


def fixing_cones(yellow, blue):
    fixed_list = []
    if len(blue) > len(yellow):
        longer_list = blue
        shorter_list = yellow
    else:
        longer_list = yellow
        shorter_list = blue
    for i in range(len(longer_list)):
        shortest_len = 10000
        best_cone = None
        for cone in range(len(shorter_list)):
            length = pythagoras((shorter_list[cone][0] - longer_list[i][0]),
                                (shorter_list[cone][1] - longer_list[i][1]))
            if length < shortest_len:
                shortest_len = length
                best_cone = shorter_list[cone]
        fixed_list.append(best_cone)
    return longer_list, fixed_list


def assessor(yellow, blue):
    """Assesses trajectories by compering lengths."""
    best_trajectory = []
    trajectory = []
    grade = 150
    yellow_fixed = fixing_cones(yellow, blue)[0]
    blue_fixed = fixing_cones(yellow, blue)[1]
    new_yellow = yellow_fixed[:11]
    new_blue = blue_fixed[:11]
    print(new_blue)
    yellow = yellow_fixed[11:]
    blue = blue_fixed[11:]
    for j in range(len(yellow) - 1):
        for i in range(2):
            # print(new_yellow)
            new_trajectory = supplier_of_trajectory(new_yellow, best_trajectory)
            new_grade = grade_based_on_time(new_yellow, new_blue, new_trajectory)
            if grade > new_grade:
                best_trajectory = new_trajectory
                # print(best_trajectory)
                grade = new_grade
            # print(best_trajectory)
        trajectory.append(best_trajectory[0])

        new_yellow.append(yellow[j + 1])
        new_yellow.pop(0)
        new_blue.append(blue[j + 1])
        new_blue.pop(0)
    return trajectory



if __name__ == '__main__':
    # print(assessor(yellow, blue))
    animation(yellow, blue)
