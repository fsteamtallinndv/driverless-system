# Driverless System 2018 #

Autonomous system for the Tallinn FS team.
Ubuntu version: ```16.04LTS amd```
ROS version: ```Kinetic```

### ROS ###
On the 2018 system we are using ROS Kinetic, which is best suitable for ubuntu 16.04LTS. If you are not used to ubuntu we reccommend dual booting into it instead of using a virtualbox.

To get ros installed you need to run the following commands. In the future there will be a possibility to use an install script
to do this automatically via a install script. At the present time it is not yet been implemented.

Setup your computer to accept software from packages.ros.org.

```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```
Setup your keys
```bash
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
```
Make sure your debian package index is up to date
```bash
sudo apt-get update
```
Desktop-Full Install: (Recommended) : ROS, rqt, rviz, robot-generic libraries, 2D/3D simulators, navigation and 2D/3D perception
```bash
sudo apt-get install ros-kinetic-desktop-full
```

Before you can use ROS, you will need to initialize rosdep. rosdep enables you to easily install system dependencies for source you want to compile and is required to run some core components in ROS.

```bash
sudo rosdep init
rosdep update
```

It's convenient if the ROS environment variables are automatically added to your bash session every time a new shell is launched:

```bash
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

### How to setup the project? ###

Make sure you are in the /src folder of the catkin_ws 

Clone the repository ... https://FormulaStudent@bitbucket.org/fsteamtallinndv/driverless-system.git

```bash
> git clone https://FormulaStudent@bitbucket.org/fsteamtallinndv/driverless-system.git
```
Run catkin build.

```bash
catkin_ws/src > catkin build 
```
If catkin build is not found.

```bash
catkin: command not found
```
Desktop-Full Install: (Recommended) : ROS, rqt, rviz, robot-generic libraries, 2D/3D simulators, navigation and 2D/3D perception

```bash
sudo apt-get install ros-kinetic-desktop-full
```
Before you can use ROS, you will need to initialize rosdep. rosdep enables you to easily install system dependencies for source you want to compile and is required to run some core components in ROS.

```bash
sudo rosdep init
rosdep update
```

It's convenient if the ROS environment variables are automatically added to your bash session every time a new shell is launched:

```bash
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

Install this package

```bash
> sudo apt-get install python-catkin-tools
```
### Contribution guidelines ###

We use feature and version based branching. When creating a branch, name it according to the base system you are going to work on following the specific feature. eg.

Committing to master is fine unntil there are no triggers build for the master branch.

```
<SYSTEM>-<COUNT OF TIMES YOU HAVE MADE A BRACH + 1>:<FEATURE NAME> 
```
Example

```
CAMERAS-7: Tracking of yellow cones
```

Additionally it would be good to create a documentation file(README.md) for every package so onboarding new people would become alot easier. 
If you are developing a completly new section for the car from scratch then create a folder, then create a ros package and procceed working in that directory.
If you are taking over a certain section development, then you will work in the specified directory.

Please develop features inside separate brances as specified above, since the master branch is linked to a build pipeline and any merges and pushes will result 
in an automatic system build, which will probably fail and waste server build time if the system is not done.

Good luck and enjoy your stay!
