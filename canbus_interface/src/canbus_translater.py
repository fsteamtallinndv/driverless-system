#!/usr/bin/env python
import rospy
from canbus_interface.msg import CanFrame
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, MultiArrayDimension
import math

# global variable to store the last received odometry reading
lastMessageReading = None



# callback function to process data from subscribed Odometry topic
def canMessageReceived(data):
    global lastMessageReading
    lastMessageReading = data



def hex_to_signed(source):
    """Convert a string hex value to a signed hexidecimal value.

    This assumes that source is the proper length, and the sign bit
    is the first bit in the first byte of the correct length.

    hex_to_signed("F") should return -1.
    hex_to_signed("0F") should return 15.
    """
    if not isinstance(source, str):
        raise ValueError("string type required")
    if 0 == len(source):
        raise valueError("string is empty")
    sign_bit_mask = 1 << (len(source)*4-1)
    other_bits_mask = sign_bit_mask - 1
    value = int(source, 16)
    return -(value & sign_bit_mask) | (value & other_bits_mask)



# main function of the node
def translateMessage():
    global lastMessageReading

    # subscribing to odometry and announcing published topics
    rospy.Subscriber("/can/data", CanFrame, canMessageReceived)
    pub_odom_data = rospy.Publisher('odom_data', Float64MultiArray, queue_size=200)

    pub_RL = rospy.Publisher('velocity_RL', Twist, queue_size=200)
    pub_RR = rospy.Publisher('velocity_RR', Twist, queue_size=200)
    pub_FR = rospy.Publisher('velocity_FR', Twist, queue_size=200)
    pub_FL = rospy.Publisher('velocity_FL', Twist, queue_size=200)

    r = rospy.Rate(200)  # an object to maintain specific frequency of a control loop - 10hz

    odom_data = Float64MultiArray()
    odom_data.layout.dim.append(MultiArrayDimension())
    odom_data.layout.dim[0].label = "RL_RR_FR_FL_angle"
    odom_data.layout.dim[0].size = 5
    odom_data.layout.dim[0].stride = 5
    odom_data.layout.data_offset = 0

    cmd_RL = Twist()
    cmd_RR = Twist()
    cmd_FR = Twist()
    cmd_FL = Twist()
    
    RL_ActualVelocity = 0
    RR_ActualVelocity = 0
    FR_ActualVelocity = 0
    FL_ActualVelocity = 0
    steering_angle = 0

    rpmToSpeed = 0.048*2/60

    while not rospy.is_shutdown():
        if lastMessageReading is None:  # we cannot issue any commands until we have our position
            print 'waiting for lastMessageReading to become available'
            r.sleep()
            continue

        canframe = lastMessageReading
        canTime = canframe.timestamp.to_sec()
        canID = canframe.arbitration_id    
        canData = canframe.data
        
        canMessage = []

        try:
            canMessage.append(list(canData)[0].encode("hex"))
            canMessage.append(list(canData)[1].encode("hex"))
            canMessage.append(list(canData)[2].encode("hex"))
            canMessage.append(list(canData)[3].encode("hex"))
            canMessage.append(list(canData)[4].encode("hex"))
            canMessage.append(list(canData)[5].encode("hex"))
            canMessage.append(list(canData)[6].encode("hex"))
            canMessage.append(list(canData)[7].encode("hex"))
        except:
            pass

        # AMK_ActualValues
        if canID == 1364:
            speed = float(hex_to_signed(canMessage[3] + canMessage[2]))*rpmToSpeed
            RL_ActualVelocity = speed

        if canID == 1366:
            speed = float(hex_to_signed(canMessage[3] + canMessage[2]))*rpmToSpeed
            RR_ActualVelocity = speed

        if canID == 1360:
            speed = float(hex_to_signed(canMessage[3] + canMessage[2]))*rpmToSpeed
            FR_ActualVelocity = speed

        if canID == 1362:
            speed = float(hex_to_signed(canMessage[3] + canMessage[2]))*rpmToSpeed
            FL_ActualVelocity = speed

        if canID == 1024:
            steering_angle = hex_to_signed(canMessage[4] + canMessage[5])

        # cmd_RL.linear.x = RL_ActualVelocity
        # #cmd_RL.linear.y = steering_angle
        # cmd_RR.linear.x = RR_ActualVelocity
        # cmd_FR.linear.x = FR_ActualVelocity
        # cmd_FL.linear.x = FL_ActualVelocity

        odom_data.data = []
        odom_data.data.append(RL_ActualVelocity)
        odom_data.data.append(RR_ActualVelocity)
        odom_data.data.append(FR_ActualVelocity)
        odom_data.data.append(FL_ActualVelocity)
        odom_data.data.append(steering_angle)

        # pub_RL.publish(cmd_RL)
        # pub_RR.publish(cmd_RR)
        # pub_FR.publish(cmd_FR)
        # pub_FL.publish(cmd_FL)

        pub_odom_data.publish(odom_data)

        r.sleep()

if __name__ == '__main__':
    rospy.init_node('can')
    translateMessage()

