#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
// Extraction specific includes
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/segmentation/extract_clusters.h>

// Probably not required
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
// voxel grid
#include <pcl/filters/voxel_grid.h>

// typedef pcl::PointXYZ PointC;
// typedef pcl::PointCloud<pcl::PointXYZ> PointCloudC;

ros::Publisher pub;



pcl::PointIndices
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud
  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromROSMsg (*input, cloud);

  pcl::ModelCoefficients coefficients;
  pcl::PointIndices inliers;
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud.makeShared ());
  seg.segment (inliers, coefficients); 
  
  // Publish the model coefficients
  return inliers;
//   pcl_msgs::ModelCoefficients ros_coefficients;
//   pcl_conversions::fromPCL(coefficients, ros_coefficients);
//   pub.publish (ros_coefficients);
}

void
lidar_master (const sensor_msgs::PointCloud2ConstPtr& input)
{
  pcl::PointIndices inliers = cloud_cb(input);
  pcl_msgs::PointIndices ros_indices;
  pcl_conversions::fromPCL(inliers, ros_indices);
  pub.publish (ros_indices);
  // mabye move to function
  // Create pointer
  pcl::PointIndices::Ptr indexes (new pcl::PointIndices (inliers));

  // Declare subset cloud
  pcl::PointCloud<pcl::PointXYZ>::Ptr subset_cloud(new pcl::PointCloud<pcl::PointXYZ>());
  
  // Convert to pcd data type
  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromROSMsg (*input, cloud);

  pcl::ExtractIndices<pcl::PointXYZ> extract;

  extract.setInputCloud(cloud.makeShared());
  extract.setIndices(indexes);
  extract.filter(*subset_cloud);
  
  // pcl_conversions::fromPCL(subset_cloud, ros_indices);
  // pub.publish (subset_cloud);

}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "sacseg");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/os1_node/points", 1, lidar_master);

  // Create a ROS publisher for the output model coefficients
  pub = nh.advertise<pcl_msgs::PointIndices> ("indic_out", 1);

  // Spin
  ros::spin ();
}