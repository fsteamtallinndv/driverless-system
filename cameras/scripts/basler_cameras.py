#!/usr/bin/env python2
import rospy
from camera import Camera
import rospy
import numpy as np
import cv2
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from subprocess import Popen, PIPE

def convertImageToRGB():
	print("Start image conversion!")
	r = rospy.Rate(60)

	right_image_pub = rospy.Publisher("right_image", Image, queue_size=1)
	left_image_pub = rospy.Publisher("left_image", Image, queue_size=1)
	error_pub = rospy.Publisher("errors", String, queue_size=30)
	bridge = CvBridge()

	right_camera = Camera("21470330")
	right_camera.Open()
	left_camera = Camera("21470323")
	left_camera.Open()

	while not rospy.is_shutdown():
		left_img = left_camera.GetImage()
		right_img = right_camera.GetImage()
		try:
			left_image_pub.publish(bridge.cv2_to_imgmsg(left_img, "bgr8"))
		except Exception as e:
			error_pub.publish("Left image not published." + str(e))
		try:
			right_image_pub.publish(bridge.cv2_to_imgmsg(right_img, "bgr8"))
		except Exception as e:
			error_pub.publish("Right image not published." + str(e))

		r.sleep()

def configureCameraComSpeed():
	sudo_password = 'nvidia'

	command = '''ifconfig eth0 mtu 9000
        ifconfig eth0 down
        ifconfig eth0 up'''.split()
	p = Popen(['sudo', '-S'] + command, stdin=PIPE, stderr=PIPE,
            universal_newlines=True)
	sudo_prompt = p.communicate(sudo_password + '\n')[1]
    
if __name__=='__main__':
	configureCameraComSpeed()
	rospy.init_node('cameras')
	convertImageToRGB()
