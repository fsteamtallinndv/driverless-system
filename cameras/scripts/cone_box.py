#!/usr/bin/env python2
from buffer import Buffer

class ConeBox:
    """Class for tracking bounding boxes."""
    def __init__(self, x, y, w, h, x_len=3, y_len=3, w_len=5, h_len=5):
        self.x = Buffer(x, length=x_len)
        self.y = Buffer(y, length=y_len)
        self.w = Buffer(w, length=w_len)
        self.h = Buffer(h, length=h_len)
        self.update(x, y, w, h)

    def update(self, x, y, w, h):
        self.x.update(x)
        self.y.update(y)
        self.w.update(w)
        self.h.update(h)

    def box(self):
        return self.x.last_out, self.y.last_out, self.w.last_out, self.h.last_out