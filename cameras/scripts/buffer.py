#!/usr/bin/env python2
from collections import deque
import numpy

class Buffer:
    """Buffer class."""
    
    def __init__(self, value, length=9, allowed_error=15):
        """
        Initialize the buffer.

        Initializes the buffer container as containing zeros.
        Initializes the last returned value as zero.

        Inputs:
        length -- number of values stored in a buffer
        allowed_error -- Maximum allowed difference between the measurment and the previous average.
                         This should in the range of 0 to 1.
        """
        self.length = length
        self.buffer = deque(maxlen=length)
        self.allowed_error = allowed_error
        self.last_out = 0
        self.std = 0
        self.update(value)

    def update(self, value):
        """
        Update the buffer and return a value.

        Inputs:
        value -- The new number to be added to the buffer.

        Outputs:
        1. value -- The average of measurments or the last valid output.
        """
        self.buffer.append(value)
        median_value = numpy.median(numpy.array(self.buffer))
        if abs(value - median_value) < self.allowed_error:
            self.last_out = median_value
            self.std = numpy.std(self.buffer)
        return self.last_out

    def __repr__(self):
        """Represent the buffer."""
        return str(self.last_out)