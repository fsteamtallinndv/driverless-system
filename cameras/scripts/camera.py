#!/usr/bin/env python2
import cv2
import numpy as np
import pypylon.pylon as py
import rospy
from time import sleep
from threading import Lock
import inspect
from std_msgs.msg import String


def display_dirs(some_dir, count=0):
    try:
        if some_dir.GetAccessMode() == 4:
            print "\t" * (count + 1), "value:", some_dir.GetValue()
    except Exception as e:
        pass
    if count < 1:
        sub_dirs = inspect.getmembers(some_dir)
        for sub_dir in sub_dirs:
            if sub_dir[0][0] != "_":
                print "\t" * (count + 1), sub_dir[0]
                display_dirs(sub_dir[1], count + 1)


class Camera:
    """
    Camera wrapper for Basler camera.
    To work correctly the camera has to be able to set binning and use BayerGB8 PixelFormat
    Camera is thread save.
    """

    def __init__(self, serialNumber):
        """
        Initializes a Camera with the given serial number
        """
        self.cam = None
        self.serial = serialNumber
        device_info = py.DeviceInfo()
        device_info.SetSerialNumber(serialNumber)
        self.error_pub = rospy.Publisher("errors", String, queue_size=30)
        for i in range(30):
            try:
                self.cam = py.InstantCamera(py.TlFactory.GetInstance().CreateDevice(device_info))
                break
            except:
                self.error_pub.publish(
                    "Camera not connected, waiting: " + str(30 - i) + " Serial number: " + self.serial)
                rospy.sleep(1)
        if self.cam is None:
            self.error_pub.publish("Camera not connected. Serial number: " + self.serial)
        self.lock = Lock()

    def Open(self):
        """
        Opens the camera and sets params.
        """
        try:
            self.lock.acquire()
            self.cam.Open()

            self.cam.Width.Value = 780
            self.cam.Height.Value = 580

            self.cam.PixelFormat.SetIntValue(17301515)  # Set image format to BayerBG8
            self.cam.ExposureTimeAbs.SetValue(8000)
            self.cam.AcquisitionFrameRateAbs.SetValue(30)
            self.cam.AcquisitionFrameRateEnable.SetValue(True)
            self.cam.AutoFunctionProfile.SetValue("ExposureMinimum")

            print "Name: " + self.cam.GetDeviceInfo().GetFullName()
            print "PixelColorValue: " + str(self.cam.PixelFormat.GetValue())
            print "ExposureMode: " + self.cam.ExposureMode.GetValue()
            print "ExposureTimeAbs: " + str(self.cam.ExposureTimeAbs.GetValue())
            print "ResultingFrameRate: " + str(self.cam.ResultingFrameRateAbs.GetValue())
            print "Acquisition mode:", self.cam.AcquisitionMode.GetValue()
            print "Acquisition status:", self.cam.AcquisitionStatus.GetValue()

            self.cam.StartGrabbing(py.GrabStrategy_LatestImageOnly)
            self.lock.release()
        except Exception as e:
            self.error_pub.publish("Failed to open camera. Serial number: " + self.serial + str(e))

    def IsOpen(self):
        """
        Checks if the Camera is open
        """
        return self.cam.IsOpen()

    def Close(self):
        """
        Closes the Camera.
        """
        self.cam.Close()

    def GetImage(self):
        img = None
        try:
            grab_result = self.cam.RetrieveResult(1000, py.TimeoutHandling_ThrowException)
            if grab_result.GrabSucceeded():
                img = grab_result.Array
            else:
                self.error_pub.publish("Image grab failed. Serial number: " + self.serial)
                return None
        except:
            self.error_pub.publish("Image grab failed. Is camera connected? Serial number: " + self.serial)
            self.__init__(self.serial)
            self.Open()
            return None

        return np.array(cv2.cvtColor(img, cv2.COLOR_BayerBG2RGB), dtype=np.uint8)

    def __del__(self):
        """
        destructor closes the Camera.
        """
        self.cam.Close()
