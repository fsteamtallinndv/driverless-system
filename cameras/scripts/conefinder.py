#!/usr/bin/env python2
import cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import imutils
import rospy
import sys, os
from sensor_msgs.msg import Image
# from cameras.msg import camera_cone
from std_msgs.msg import String
from main.msg import ConeLocations
import math
import time
from stereo_handler import StereoHandler
from cone_box import ConeBox
import traceback

class ConeFinder:
	"""Class for finding cones from an image."""
	def __init__(self, write_video=False, gui=False):
		self.error_pub = rospy.Publisher("errors", String, queue_size=30)

		# self.cone_pub = rospy.Publisher("camera_cone", camera_cone, queue_size=30)
		self.message_pub = rospy.Publisher("cone_locations", ConeLocations, queue_size=30)

		self.debug_counter = 0
		self.write_video = write_video
		self.gui = gui
		self.left_AOI_path = os.path.join(os.path.expanduser('~'), 'catkin_ws', 'src', 'driverless-system', 'cameras', 'scripts', 'left_AOI.png')
		if not os.path.exists(self.left_AOI_path):
		    self.error_pub.publish("Path to left AOI image is wrong. Image not found at:" + self.left_AOI_path)
		self.left_AOI = cv2.imread(self.left_AOI_path)
		self.left_AOI = cv2.cvtColor(self.left_AOI, cv2.COLOR_BGR2GRAY)
		_, self.left_AOI = cv2.threshold(self.left_AOI, thresh=10, maxval=255, type=cv2.THRESH_BINARY)
		if self.write_video:
			fourcc = cv2.VideoWriter_fourcc(*'mp4v')
			self.video = cv2.VideoWriter("out.mp4", fourcc, 30.0, (780, 580))

		self.current_message = None
		self.yellow_index = chr(1)
		self.blue_index = chr(2)

		self.d0 = 0
		self.d1 = 75
		self.image = None

		self.real_width = 228 # mm
		self.real_height = 325 # mm
		self.ratio = self.real_width / self.real_height

		self.focal_length = 6 # mm
		self.frame_height = 580 # px
		self.frame_width = 780 # px
		self.sensor_height = 5.427 # mm
		self.sensor_width = 6.784 # mm

		self.height = 0.9 # m

		self.stereo = StereoHandler()

		self.vertical_direction = math.radians(16.5)
		self.vertical_angle = 2 * math.atan(self.sensor_height / 2 / self.focal_length)
		self.horizontal_angle = 2 * math.atan(self.sensor_width / 2 / self.focal_length)
		self.distance_multiplier = math.cos(self.vertical_direction)

		self.yellow_boxes_left = set()
		self.blue_boxes_left = set()
		self.yellow_boxes_right = set()
		self.blue_boxes_right = set()
		self.color_y_px = None
		self.color_b_px = None
		self.left_image = None
		self.left_hsv_image = None
		self.left_gray = None
		self.right_image = None
		self.right_hsv_image = None
		self.right_gray = None
		self.disparity = None

	def update_left(self, image):
		self.left_image = image
		self.left_hsv_image = cv2.cvtColor(self.left_image, code=cv2.COLOR_BGR2HSV)
		self.left_gray = cv2.cvtColor(self.left_image, code=cv2.COLOR_BGR2GRAY)

	def update_right(self, image):
		self.right_image = image
		self.right_hsv_image = cv2.cvtColor(self.right_image, code=cv2.COLOR_BGR2HSV)
		self.right_gray = cv2.cvtColor(self.right_image, code=cv2.COLOR_BGR2GRAY)

	def detect(self):
		"""Detect cones."""
		self.time = time.time() # OPTIMIZATION

		y_contours = []
		b_contours = []
		self.current_message = ConeLocations()

		if self.left_gray is not None and self.right_gray is not None:
			self.stereo_detect()

		#try:
		#	self.stereo.to_test(self.left_gray, self.right_gray)
		#except Exception as e:
		#	print traceback.print_exc()

		if self.left_hsv_image is not None:
			self.color_detect(self.left_hsv_image)
			yellow_cone_px = self.color_y_px
			blue_cone_px = self.color_b_px

			_, y_contours, _ = cv2.findContours(yellow_cone_px, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Contours for yellow cones
			_, b_contours, _ = cv2.findContours(blue_cone_px, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Contours for blue cones
			cone_pixels = cv2.bitwise_or(yellow_cone_px, blue_cone_px) # Cone pixels

			if self.gui:
				self.to_display = cv2.addWeighted(self.left_image, 0.5, cv2.bitwise_and(self.left_image, self.left_image, mask=cone_pixels), 0.5, 0)

			self.handle_contours(y_contours, self.yellow_index)
			self.handle_contours(b_contours, self.blue_index)

			if self.gui:
				try:
					cv2.imshow('to_display', self.to_display)
				except:
					pass
				try:
					# cv2.imshow('left_gray', self.left_gray)
					pass
				except:
					pass
				try:
					# cv2.imshow('right_gray', self.right_gray)
					pass
				except:
					pass
				try:
					# cv2.imshow('disparity', self.disparity).
					pass
				except:
					pass

				key = cv2.waitKey(1)
				if key & 0xFF == ord('q'):
					self.d0 += 1
					print "d0", self.d0
				elif key & 0xFF == ord('a'):
					self.d0 -= 1
					print "d0", self.d0
				elif key & 0xFF == ord('w'):
					self.d1 += 1
					print "d1", self.d1
				elif key & 0xFF == ord('s'):
					self.d1 -= 1
					print "d1", self.d1
				elif key & 0xFF == ord('c'):
					global pick_good
					global good_color_locs
					global bad_color_locs
					calibration = self.left_image.copy()
					while True:
						for point in good_color_locs:
							cv2.circle(calibration, point, 3, (0, 115, 0))
						for point in bad_color_locs:
							cv2.circle(calibration, point, 3, (0, 0, 115))
						cv2.imshow('image', calibration)
						cv2.setMouseCallback('image', get_good_color)

						key = cv2.waitKey(1)
						if key & 0xFF == ord('q'):
							break
						elif key & 0xFF == ord('g'):
							print good_color_locs
							pick_good = True
						elif key & 0xFF == ord('b'):
							print bad_color_locs
							pick_good = False
						elif key & 0xFF == ord('r'):
							good_color_locs = []
							bad_color_locs = []
							calibration = self.left_image.copy()
						elif key & 0xFF == ord('w'):
							# good_colors = list(map(lambda loc: self.left_hsv_image[loc[1]][loc[0]], good_color_locs))
							# print "colors: ", good_colors
							cv2.imwrite('left' + str(self.d0) + '.png', self.left_gray)
							cv2.imwrite('right' + str(self.d0) + '.png', self.right_gray)
							cv2.imwrite('disparity' + str(self.d0) + '.png', self.disparity)
						elif key & 0xFF == ord('s'):
							pass

		if self.right_hsv_image is not None:
			self.color_detect(self.right_hsv_image)
			yellow_cone_px = self.color_y_px
			blue_cone_px = self.color_b_px

			_, y_contours, _ = cv2.findContours(yellow_cone_px, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Contours for yellow cones
			_, b_contours, _ = cv2.findContours(blue_cone_px, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Contours for blue cones
			cone_pixels = cv2.bitwise_or(yellow_cone_px, blue_cone_px) # Cone pixels

			self.handle_contours(y_contours, self.yellow_index)
			self.handle_contours(b_contours, self.blue_index)

		self.message_pub.publish(self.current_message)

		if self.write_video:
			self.video.write(self.to_display)

	def handle_contours(self, contours, cnt_type):
		for contour in contours:
			x,y,w,h = cv2.boundingRect(contour)
			ratio_error = (w / h - self.ratio)
			area = w * h
			if abs(ratio_error) < 0.3 and area >= 100:
				location = self.locate((x, y, w, h))
				self.current_message.ranges.append(location[0])
				self.current_message.bearings.append(location[1])
				self.current_message.types += cnt_type
				if self.gui and self.left_hsv_image is not None:
					cv2.rectangle(self.to_display, (x, y), (x + w, y + h) , (0, 255, 255), 1)
					cv2.putText(self.to_display, str(location), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.4 , (0, 0, 255), 1)

	def color_detect(self, hsv_image):
		b0m = cv2.inRange(hsv_image, lowerb=np.array([0, 0, 0]), upperb=np.array([179, 255, 50])) # black
		b1m = cv2.inRange(hsv_image, lowerb=np.array([15, 64, 64]), upperb=np.array([30, 255, 255])) # yellow

		b3m = cv2.inRange(hsv_image, lowerb=np.array([0, 0, 220]), upperb=np.array([179, 128, 255])) # white
		b4m = cv2.inRange(hsv_image, lowerb=np.array([105, 128, 64]), upperb=np.array([115, 255, 255])) # blue

		b0m = cv2.bitwise_and(b0m, self.left_AOI) # Black in AOI
		b1m = cv2.bitwise_and(b1m, self.left_AOI) # Yellow in AOI
		
		b3m = cv2.bitwise_and(b3m, self.left_AOI) # Black in AOI
		b4m = cv2.bitwise_and(b4m, self.left_AOI) # Blue in AOI
		b1m = cv2.erode(b1m, kernel=np.array(
			[[0, 1, 0],
			[1, 1, 1],
			[0, 1, 0]], 
			np.uint8), iterations=1) # Remove yellow noise
		b1m = cv2.dilate(b1m, kernel=np.array(
			[[0, 1, 0],
			[1, 1, 1],
			[0, 1, 0]], 
			np.uint8), iterations=1) # Yellow to original size
		
		b4m = cv2.erode(b4m, kernel=np.array(
			[[0, 1, 0],
			[1, 1, 1],
			[0, 1, 0]],
			np.uint8), iterations=1) # Remove blue noise
		b4m = cv2.dilate(b4m, kernel=np.array(
			[[0, 1, 0],
			[1, 1, 1],
			[0, 1, 0]], 
			np.uint8), iterations=1) # Blue to original size

		b0m = b1m
		b3m = b4m

		b0m = cv2.dilate(b0m, np.array(
			[[1, 1, 1],
			[0, 1, 0],
			[0, 1, 0]],
			np.uint8), iterations=10) # Fill and smooth yellow cones
		b0m = cv2.erode(b0m, np.array(
			[[0, 1, 0],
			[0, 1, 0],
			[1, 1, 1]],
			np.uint8), iterations=10) # Yellow cones back to original size
		
		b3m = cv2.dilate(b3m, np.array(
			[[1, 1, 1],
			[0, 1, 0],
			[0, 1, 0]],
			np.uint8), iterations=10) # Fill and smooth blue cones
		b3m = cv2.erode(b3m, np.array(
			[[0, 1, 0],
			[0, 1, 0],
			[1, 1, 1]],
			np.uint8), iterations=10) # Blue cones back to original size
		self.color_debug = b3m
		
		self.color_y_px = b0m
		self.color_b_px = b3m

	def stereo_detect(self):
		self.disparity = self.stereo.process(self.left_gray, self.right_gray)
		

	def locate(self, box):
		bad_multiplier = 1 # <-- TODO: REMOVE THIS BAD MULTIPLIER. THIS IS JUST TEMPORARY.
		error_multiplier = 3 # <-- TODO: REMOVE ERROR MULTIPLIER.
		distance = math.sqrt((self.focal_length**2 
							  * self.real_height * self.real_width 
							  * self.frame_height * self.frame_width)
							 / (box[2] * box[3]
							  * self.sensor_height * self.sensor_width
							  * 1000000)) * bad_multiplier
		distance *= self.distance_multiplier
		angle = -(2 * box[0] + box[2] - self.frame_width) / 2 * self.horizontal_angle / self.frame_width
		angle_std = self.horizontal_angle / self.frame_width
		max_distance = math.sqrt((self.focal_length**2 
							  * self.real_height * self.real_width 
							  * self.frame_height * self.frame_width)
							 / ((box[2] * box[3] - math.sqrt(box[2] * box[3]))
							  * self.sensor_height * self.sensor_width
							  * 1000000)) * bad_multiplier
		min_distance = math.sqrt((self.focal_length**2 
							  * self.real_height * self.real_width 
							  * self.frame_height * self.frame_width)
							 / ((box[2] * box[3] + math.sqrt(box[2] * box[3]))
							  * self.sensor_height * self.sensor_width
							  * 1000000)) * bad_multiplier
		distance_std = (max_distance - min_distance) / 2 * error_multiplier
		# y = round(math.cos(angle) * distance, 3)
		# x = round(math.sin(angle) * distance, 3)
		# y_std = abs(round(math.cos(angle) * distance_std, 3))
		# x_std = abs(round(math.sin(angle) * distance_std, 3))
		return distance, angle, distance_std, angle_std  # x, y, x_std, y_std

	def projection_locate(self, box):
		return self.height * math.tan(math.pi / 2 - self.vertical_direction + ((self.frame_height - 2 * box[1] - 2 * box[3] + 1) * self.vertical_angle / self.frame_height / 2))

<<<<<<< HEAD
rospy.init_node('listener', anonymous=True)
=======

>>>>>>> cameras
my_finder = ConeFinder(False, rospy.get_param('/camera/conefinder/gui', "false"))
bridge = CvBridge()

good_color_locs = []
bad_color_locs = []
pick_good = True


def get_good_color(event, x, y, flags, param):
	if event == cv2.EVENT_LBUTTONDOWN:
		if pick_good:
			good_color_locs.append((x, y))
		else:
			bad_color_locs.append((x, y))


def left_in(data):
	my_finder.update_left(bridge.imgmsg_to_cv2(data, "bgr8"))

def right_in(data):
	my_finder.update_right(bridge.imgmsg_to_cv2(data, "bgr8"))


def listener():
	print "launched listener"
	rospy.Subscriber("left_image", Image, left_in, queue_size=1)
	rospy.Subscriber("right_image", Image, right_in, queue_size=1)
	r = rospy.Rate(30)
	while not rospy.is_shutdown():
		my_finder.detect()
		r.sleep()


def main():
	listener()
	

if __name__ == "__main__":
	main()
