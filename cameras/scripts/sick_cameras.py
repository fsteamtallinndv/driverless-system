#!/usr/bin/env python2
import cv2
import numpy as np
import pypylon.pylon as py

import Aravis


def list_cameras_0():
    index = 0
    arr = []
    while True:
        cap = cv2.VideoCapture(index)
        if not cap.read()[0]:
            break
        else:
            arr.append(index)
        cap.release()
        index += 1
    return len(arr)

def list_cameras_1():
    tlfactory = py.TlFactory.GetInstance()
    detected_devices = tlfactory.EnumerateDevices()
    return len(detected_devices)

print "method 1 cameras found:", list_cameras_0()
print "method 2 cameras found:", list_cameras_1()

'''
cap = cv2.VideoCapture(0)
while(True):
    ret, frame = cap.read()
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
'''