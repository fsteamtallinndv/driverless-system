//Open camera with ID 1
#include <ros/ros.h>
#include <ueye.h>
HIDS hCam = 1;
INT nRet = is_InitCamera(&hCam, NULL);

using namespace std;

void camera(){
    if (nRet != IS_SUCCESS){
    //Check if GigE uEye SE needs a new starter firmware
        if(nRet == IS_STARTER_FW_UPLOAD_NEEDED){
            //For cameras with an ID other than 0, the ID must be set newly
            hCam = 1;
            //Calculate time needed for updating the starter firmware
            INT nTime;
            is_GetDuration(hCam, IS_SE_STARTER_FW_UPLOAD, &nTime);

            /*
            e.g. have progress bar displayed in separate thread
            */
            //Upload new starter firmware during initialization

            hCam =  hCam | IS_ALLOW_STARTER_FW_UPLOAD;
            nRet = is_InitCamera(&hCam, NULL);
        /*
            end progress bar
        */
        }
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "example_node");
    ros::NodeHandle n("~");
    ros::Rate loop_rate(50);
    while (ros::ok()) {
        camera();
        ros::spinOnce();
        loop_rate.sleep();
    }
}