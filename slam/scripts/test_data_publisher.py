#!/usr/bin/env python

import rospy
import numpy as np
from collections import defaultdict
import math
import time
from main.msg import ConeLocations
from main.msg import OdometryMotion
import rospkg



def read_sensor_data(filename):

    data_dict = defaultdict()
    id_arr =[]
    range_arr=[]
    bearing_arr=[]
    first_time = True
    timestamp=-1
    f = open(filename)
    for line in f:
        
        line_s = line.split('\n') # remove the new line character
        line_spl = line_s[0].split(' ') # split the line
        
        if (line_spl[0]=='ODOMETRY'):
            
            
            data_dict[timestamp,'odom'] = {'r1':float(line_spl[1]),'t':float(line_spl[2]),'r2':float(line_spl[3])}
            if (first_time == True): 
                first_time = False
                
            else: 
            
                data_dict[timestamp,'sensor'] = {'id':id_arr,'range':range_arr,'bearing':bearing_arr}                
                id_arr=[]
                range_arr = []
                bearing_arr = []

            timestamp = timestamp+1
           
                
            
        if(line_spl[0] == 'SENSOR'):
            
            id_arr.append(int(line_spl[1]))    
            range_arr.append(float(line_spl[2]))
            bearing_arr.append(float(line_spl[3]))
                              
            
    data_dict[timestamp-1,'sensor'] = {'id':id_arr,'range':range_arr,'bearing':bearing_arr}            
    return data_dict



def read_send_data():
    # Test data location
    
    rospack = rospkg.RosPack()
    print rospack.get_path('slam')
    
    data_dict = read_sensor_data(rospack.get_path('slam') + '/data/sensor_data.dat') 

    pub_meas = rospy.Publisher('cone_locations', ConeLocations, queue_size=10000)
    
    r = rospy.Rate(8)

    pub_odom = rospy.Publisher('odom_motion', OdometryMotion, queue_size=10000)
    
    measurements = ConeLocations()

    odometry = OdometryMotion()

    t = 0
    
    # Infinite loop until shutdown
    while not rospy.is_shutdown():

        if t <= len(data_dict)/2:
            odom = data_dict[t,'odom']
            ranges = data_dict[t,'sensor']['range']
            bearings = data_dict[t,'sensor']['bearing']

            ids = data_dict[t, 'sensor']['id']

            measurements.ranges = ranges
            measurements.bearings = bearings

            measurements.types = []
            for i in ids:
                if i == 1 or i == 2 or i == 3:
                    measurements.types.append(1) # yellow
                elif i == 4 or i == 5:
                    measurements.types.append(2) # blue
                elif i == 6:
                    measurements.types.append(3) # small orange
                elif i == 7:
                    measurements.types.append(4) # big orange
                else:
                    measurements.types.append(0) # N/A

            odometry.rotation1 = odom['r1']
            odometry.translation = odom['t']
            odometry.rotation2 = odom['r2']
        
            pub_odom.publish(odometry)
            pub_meas.publish(measurements)

            t += 1

        r.sleep()


## Main loop Starts here
if __name__ == '__main__':
    print "Test data publisher started"
    #Initialize node
    rospy.init_node('slam_test_data_publisher')
    
    time.sleep(1)

    read_send_data()


