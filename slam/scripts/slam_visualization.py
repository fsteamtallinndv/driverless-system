#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped, PoseArray, Pose, Twist, Point
from main.msg import ConeLocations,  OdometryMotion, Map2D
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, ColorRGBA
from visualization_msgs.msg import MarkerArray, Marker
import tf
import math 
from math import *


# callback functions to process data from subscribed topics
def poses_received(data):
    global last_poses_reading
    last_poses_reading = data



def trajectory_received(data):
    global last_trajectory_reading
    last_trajectory_reading = data



def map_received(data):
    global last_map_reading
    last_map_reading = data



def measurements_received(data):
    global last_meas_reading
    last_meas_reading = data



def pose_received(data):
    global last_pose_reading
    last_pose_reading = data



def make_pose_array_msg(msg, array):
    # Visualize particles
    msg.header.seq += 1
    msg.header.stamp = rospy.Time.now()

    width = array.layout.dim[1].size

    for i in range(array.layout.dim[0].size):
        step = Pose()
        step.position.x = array.data[i*width]
        step.position.y = array.data[i*width + 1]

        quaternion = tf.transformations.quaternion_from_euler(0, 0, array.data[i*width + 2])
        step.orientation.x = quaternion[0]
        step.orientation.y = quaternion[1]
        step.orientation.z = quaternion[2]
        step.orientation.w = quaternion[3]

        msg.poses.append(step) 

    return msg



def visualize():
    # Initialization to read data
    global last_poses_reading
    global last_trajectory_reading
    global last_map_reading
    global last_meas_reading
    global last_pose_reading

    last_poses_reading = None
    last_trajectory_reading = None
    last_map_reading = None
    last_meas_reading = None
    last_pose_reading = None

    r = rospy.Rate(20)  

    # Subscribed messages
    rospy.Subscriber("p_poses", Float64MultiArray, poses_received)
    rospy.Subscriber("trajectory", Float64MultiArray, trajectory_received)
    rospy.Subscriber("map", Float64MultiArray, map_received)
    rospy.Subscriber("cone_locations", ConeLocations, measurements_received)
    rospy.Subscriber('pose', PoseStamped, pose_received)


    # Published messages

    pub_p_poses = rospy.Publisher('visualize_p_poses', PoseArray, queue_size=10)
    p_poses = PoseArray()
    p_poses.header.frame_id = "base_link"
    p_poses.header.seq = 0
    p_poses.header.stamp = rospy.Time.now()

    pub_trajectory = rospy.Publisher('visualize_trajectory', PoseArray, queue_size=10)
    trajectory = PoseArray()
    trajectory.header.frame_id = "base_link"
    trajectory.header.seq = 0
    trajectory.header.stamp = rospy.Time.now()

    pub_map = rospy.Publisher('visualize_map', MarkerArray, queue_size=10)
    cone_map = MarkerArray()

    pub_meas = rospy.Publisher('visualize_measurements', Marker, queue_size=10)
    meas = Marker()
    meas.header.frame_id = "base_link"
    meas.id = 0
    meas.type = meas.LINE_LIST
    meas.scale.x = 0.3
    meas.header.seq = 0
    meas.action = meas.MODIFY
    meas.lifetime = rospy.Duration()

    # Infinite loop until shutdown
    while not rospy.is_shutdown():

        if last_poses_reading is not None:
            # Visualize particles
            pub_p_poses.publish(make_pose_array_msg(p_poses, last_poses_reading))
            p_poses.poses = []

            last_poses_reading = None


        if last_trajectory_reading is not None:
            # Visualize trajectory
            pub_trajectory.publish(make_pose_array_msg(trajectory, last_trajectory_reading))
            trajectory.poses = []

            last_trajectory_reading = None

        
        if last_map_reading is not None:
            # Visualize cones on rviz
            width = last_map_reading.layout.dim[1].size

            cone_map = []


            for i in range(last_map_reading.layout.dim[0].size):
                step = Marker()
                step.pose.position.x = last_map_reading.data[i*width]
                step.pose.position.y = last_map_reading.data[i*width + 1]
                step.id = last_map_reading.data[i*width + 3]
                
                step.header.frame_id = "base_link"
                
                step.type = step.CYLINDER
                step.action = step.MODIFY
                t = rospy.Duration()
                step.lifetime = t
                step.scale.x = 0.3
                step.scale.y = 0.3
                step.color.a = 1.0
                
                step.header.seq = 0
                step.header.stamp = rospy.Time.now()

                if last_map_reading.data[i*width + 2] == 1: # yellow
                    step.color.r = 1
                    step.color.g = 1
                    step.color.b = 0
                    step.scale.z = 0.5

                elif last_map_reading.data[i*width + 2] == 2: # blue
                    step.color.r = 0
                    step.color.g = 0
                    step.color.b = 1
                    step.scale.z = 0.5

                elif last_map_reading.data[i*width + 2] == 3: # small orange
                    step.color.r = 1
                    step.color.g = 0.647
                    step.color.b = 0
                    step.scale.z = 0.5

                elif last_map_reading.data[i*width + 2] == 4: # big orange
                    step.color.r = 1
                    step.color.g = 0.647
                    step.color.b = 0
                    step.scale.z = 1

                elif last_map_reading.data[i*width + 2] == 0: # N/A
                    step.color.r = 0.5
                    step.color.g = 0.5
                    step.color.b = 0.5
                    step.scale.z = 0.5

                cone_map.append(step) 

            pub_map.publish(cone_map)

            last_map_reading = None


        if last_meas_reading is not None and last_pose_reading is not None:
            # Visualize measurements with lines
            
            robot_pose = last_pose_reading.pose.position
            q = last_pose_reading.pose.orientation
            euler = tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
            robot_theta = euler[2]
            start = Point()
            
            start = robot_pose

            meas.points = []
            meas.colors = []

            for i in range(len(last_meas_reading.ranges)):
                end = Point()
                end.x = robot_pose.x + last_meas_reading.ranges[i] * cos(robot_theta + last_meas_reading.bearings[i])
                end.y = robot_pose.y + last_meas_reading.ranges[i] * sin(robot_theta + last_meas_reading.bearings[i])
                
                line_color = ColorRGBA()
                line_color.a = 1.0

                if last_meas_reading.types[i] == 1: # yellow
                    line_color.r = 1
                    line_color.g = 1
                    line_color.b = 0
                    meas.scale.x = 0.01

                elif last_meas_reading.types[i] == 2: # blue
                    line_color.r = 0
                    line_color.g = 0
                    line_color.b = 1
                    meas.scale.x = 0.01

                elif last_meas_reading.types[i] == 3: # small orange
                    line_color.r = 1
                    line_color.g = 0.647
                    line_color.b = 0
                    meas.scale.x = 0.01

                elif last_meas_reading.types[i] == 4: # big orange
                    line_color.r = 1
                    line_color.g = 0.647
                    line_color.b = 0
                    meas.scale.x = 0.02

                elif last_meas_reading.types[i] == 0: # N/A
                    line_color.r = 0.5
                    line_color.g = 0.5
                    line_color.b = 0.5
                    meas.scale.x = 0.01
                    

                meas.points.append(start)
                meas.points.append(end)
                meas.colors.append(line_color)
                meas.colors.append(line_color)
            
            meas.header.stamp = rospy.Time.now()
            pub_meas.publish(meas)

            last_meas_reading = None
            last_pose_reading = None


        r.sleep()



## Main loop Starts here
if __name__ == '__main__':

    #Initialize node
    rospy.init_node('slam_visualization')

    visualize()