#!/usr/bin/env python

import rospy
import numpy as np
from math import *
import math 
from geometry_msgs.msg import PoseStamped, Pose, Twist, TransformStamped
from nav_msgs.msg import Odometry
from main.msg import OdometryMotion
from std_msgs.msg import Float64MultiArray
import tf




# Input parameter for speed calculations
m_f = 92 # expected static mass in front [kg]
m_r = 119 # expected static mass in rear [kg]
m = m_f + m_r # full mass

WB = 1.53 # FEST18 Wheelbase [m]

l_f = WB * (m_r / m) # distance to front axle from CoM
l_r = WB * (m_f / m) # distance to rear axle from CoM


# Steering angle parameters

steering_angle_threshold = 0.01

# Outside wheel turn angle in degrees
out_wheel_angles = np.array([0,    2,    4,    6,    8,    10,   12,   14,   16,   18,   20,   22])

# Offset from outside wheel to get inside wheel turn angle
out_in_offsets = np.array([0,    0.04, 0.16, 0.37, 0.67, 1.08, 1.60, 2.25, 3.05, 4.03, 5.21, 6.64])

# Inside wheel turn angle in degrees
in_wheel_angles = out_wheel_angles + out_in_offsets

# Steering ratio to convert steering wheel angle from data reading to actual left and right wheel position of vehicle
steering_ratios = np.array([6.98, 6.92, 6.77, 6.61, 6.43, 6.24, 6.03, 5.80, 5.55, 5.27, 4.96, 4.62])

# Parameter what is used to get actual steering ratio to calculate in wheel and out wheel angles in corner
out_steering_wheel_angles = np.multiply(out_wheel_angles, steering_ratios) 



# Translatse rpm unit to m/s. Constant includes FEST18 wheel size, constant pi and gearbox ratio.
def rpm_conversion(rpm):
    return  rpm * 0.048*2/60



# Converting steering wheel position to actual wheel angle using table, where ackermann is counted in
# At the moment returns avg of left and right wheel angle TODO: Should be reconsidered!
def steering_angle_conversion(steering_angle_input):
    steer_angle = abs(steering_angle_input)

    #Checks minimum threshold
    if steer_angle < steering_angle_threshold:
        return 0

    else:

        for i in range(len(out_steering_wheel_angles)):

            # Can't be equal to zero because of threshold, therefore first comparison won't work anyway 
            # and I can use list value [i-1] for interpolation
            if steer_angle <= out_steering_wheel_angles[i]:

                delta_steering_wheel_angle = out_steering_wheel_angles[i] - out_steering_wheel_angles[i - 1]
                delta = (steer_angle - out_steering_wheel_angles[i - 1]) / delta_steering_wheel_angle # Difference what is needs to be added [i-1] value to get interpolated values

                delta_steering_ratio = steering_ratios[i] - steering_ratios[i - 1]
                interpolated_ratio = steering_ratios[i - 1] + (delta * delta_steering_ratio) # Ratio, I am looking for to get actual angle

                out_wheel_angle = steer_angle / interpolated_ratio

                delta_out_in_offset = out_in_offsets[i] - out_in_offsets[i - 1]
                interpolated_offset = out_in_offsets[i - 1] + (delta * delta_out_in_offset) # Offset I need to use to get in wheel angle

                in_wheel_angle = out_wheel_angle + interpolated_offset

                # Vehicle turns right, negative value
                if steering_angle_input < 0:
                    return math.radians((-1) * (((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

                # Vehicle turns left, positive value
                else:
                    return math.radians((((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

        # This case steering angle is out of maximum limited steering position,
        # Program will use maximum parameters from table to convert this value
        # Normally program wont reach here!
        out_wheel_angle = steer_angle / steering_ratios[len(steering_ratios) - 1]
        in_wheel_angle = out_wheel_angle + out_in_offsets[len(out_in_offsets) - 1]

        # Vehicle turns right, negative value
        if steering_angle_input < 0:
            return math.radians((-1) * (((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

        # Vehicle turns left, positive value
        else:
            return math.radians((((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))



# Convert steering angle and wheel speeds to linear and angular velocity [m/s]
# TODO: Test if tangens function is working properly and if pos/neg of omega is correct
def speeds_calculation(FR, FL, RL, RR, steering_angle):
    # NB! Check global variables to get all inputs for function!
    #       Steering angle needs to be converted using ackermann and turning ratio.

    # Input requirements 
    #   - Steering angle should be in radians. NB! It is not actual reading from sensor data. Ackermann and ratio needs to be considered (with lookup table)
    #   - Speeds FR, FL ... should be in meters per second [m/s]

    # Output requirements
    #   - Point mass linear speed is in meters per second [m/s]
    #   - Point mass angular velocity is in radians per second [1/s]


    v_f = (FR + FL) / 2 # front wheel speeds in bicycle model [m/s]
    v_r = (RR + RL) / 2 # rear wheel speeds in bicycle model [m/s]

    # Calculations: Using only rear wheel speed for omega calculation.
    # Probably should count front wheels also but rear wheels should be more safe in terms of wheel slip
    if (abs(steering_angle) > steering_angle_threshold):
        R_r = WB / tan(steering_angle)
        R = sqrt( R_r**2 + l_r**2 )
        omega = v_r / R_r

        if steering_angle > 0:
            speed = omega * R
        else:
            speed = omega * R * (-1)
            
    #Takes average of all four wheels for speed calculation
    else:
        speed = (v_f + v_r) / 2
        omega = 0

    

    return [speed, omega]

    # Unknown input parameters -> need specification
    # c_f = 100000 * 2 * (m_r / m) # N/rad
    # c_r = 100000 * 2 * (m_f / m) # N/rad

    # mu = 0.9 # depends on track

    # # Unknown parameters
    # v, omega, r, alfa_f, alfa_r, beeta, a_y

    # # Equations
    # v = omega * r 

    # a_y = v**2 / r

    # steering_angle = (WB / r) + abs(alfa_f - alfa_r)

    # alfa_f = beeta - (omega * l_f / v) + steering_angle # angle between steering angle and real front wheel movement vector in corner

    # alfa_r = beeta + (omega * l_f / v) # angle between rear wheel and real rear wheel movement vector in corner

    # alfa_f = a_y * ( m_f * 9.81 ) / c_f

    # alfa_r = a_y * ( m_r * 9.81 ) / c_r



def odom_data_received(data):
    global last_odom_data_reading
    last_odom_data_reading = data



def odometry():
    global last_odom_data_reading

    last_odom_data_reading = None

    rospy.Subscriber("odom_data", Float64MultiArray, odom_data_received)

    pub_odom = rospy.Publisher('odom', Odometry, queue_size=10)
    odom = Odometry()
    odom.header.frame_id = "odom"
    odom.child_frame_id = "base_link"


    pub_odom_trans = rospy.Publisher('odom_trans', TransformStamped, queue_size=10)
    odom_trans = TransformStamped()
    odom_trans.header.frame_id = "odom"
    odom_trans.child_frame_id = "base_link"

    r = rospy.Rate(100)

    x = 0.0
    y = 0.0
    th = 0.0

    vx = 0.0
    vy = 0.0
    vth = 0.0

    last_time = rospy.Time.now()
    current_time = rospy.Time.now()

    while not rospy.is_shutdown():
        #Publish odometry
        if last_odom_data_reading is not None:
            current_time = rospy.Time.now()
            dt = (current_time - last_time).to_sec()

            # Speed in m/s and angle in degrees
            RL = last_odom_data_reading.data[0]
            RR = last_odom_data_reading.data[1]
            FR = last_odom_data_reading.data[2]
            FL = last_odom_data_reading.data[3]

            # Magic constant should theoredically be 10 to get result in degrees, but added value to get better result on snow test data
            steering_angle = steering_angle_conversion(last_odom_data_reading.data[4] / -12.9)

            [vx, vth] = speeds_calculation(FR, FL, RL, RR, steering_angle)

            #rospy.logerr(str(RL) + " " + str(RR) + " " + str(FR) + " " + str(FL) + " " + str(vx) + " " + str(vth))

            delta_x = (vx * cos(th) - vy * sin(th)) * dt
            delta_y = (vx * sin(th) + vy * cos(th)) * dt
            delta_th = vth * dt

            x += delta_x
            y += delta_y
            th += delta_th     

            odom_quat = tf.transformations.quaternion_from_euler(0, 0, th)

            # Publish transform over tf
            odom_trans.header.stamp = current_time
            odom_trans.transform.translation.x = x
            odom_trans.transform.translation.y = y
            odom_trans.transform.translation.z = 0.0
            #odom_trans.transform.rotation = odom_quat
            odom_trans.transform.rotation.x = odom_quat[0]
            odom_trans.transform.rotation.y = odom_quat[1]
            odom_trans.transform.rotation.z = odom_quat[2]
            odom_trans.transform.rotation.w = odom_quat[3]

            pub_odom_trans.publish(odom_trans)

            # Publish odometry message over ROS
            odom.header.stamp = current_time
            odom.pose.pose.position.x = x
            odom.pose.pose.position.y = y
            odom.pose.pose.position.z = 0.0
            #odom.pose.pose.orientation = odom_quat
            odom.pose.pose.orientation.x = odom_quat[0]
            odom.pose.pose.orientation.y = odom_quat[1]
            odom.pose.pose.orientation.z = odom_quat[2]
            odom.pose.pose.orientation.w = odom_quat[3]

            odom.twist.twist.linear.x = vx
            odom.twist.twist.linear.y = vy
            odom.twist.twist.angular.z = vth

            pub_odom.publish(odom)

            last_time = current_time
            last_odom_data_reading = None

        r.sleep()



## Main loop Starts here
if __name__ == '__main__':

    #Initialize node
    rospy.init_node('odometry_publisher')

    odometry()