#!/usr/bin/env python

import rospy
from math import *
import math 
import numpy as np
import random
from collections import defaultdict
import matplotlib.pyplot as plt
import cv2
import time
import copy
from geometry_msgs.msg import PoseStamped, PoseArray, Pose, Twist
from main.msg import ConeLocations,  OdometryMotion, Map2D
import tf
from std_msgs.msg import Float64MultiArray, MultiArrayDimension
from nav_msgs.msg import Odometry

# Define parameters

# Consider number of effective particles, to decide whether to resample or not
useNeff = True

noise_param = np.array([0.005, 0.01, 0.005]).T

default_particle_number = 10




# Functions


''' Normalize the angle between -pi and pi'''       
def normalize_angle(angle):
    if(len(angle)>1):
        angle=angle[0]
    
    while(angle > math.pi):
        angle -= 2*math.pi
    
    while (angle < - math.pi):
        angle += 2*math.pi
        
    return angle


# Matrix calculations for better code readability

def transpose(x):
    return np.array(x).T

def inverse(x):
    return np.linalg.inv(x)

def matmul(a, b):
    return np.matmul(a, b)
        
def det(x):
    return np.linalg.det(x)

def eye(x):
    return np.eye(x)



# extract position from a particle set
def get_position(p):
    x = 0.0
    y = 0.0
   
    orientation=0

    for i in range(len(p)):
        x += p[i].pose[0]
        y += p[i].pose[1]   
        
        orientation += p[i].pose[2]
    
    avg_orient = normalize_angle(orientation / len(p))
     
    return [x / len(p), y / len(p), avg_orient ]




def getKey(item):
    return np.argmax(item[3])


# particle class
class robot():

    # init: 
    #    creates robot and initializes location/orientation 
    def __init__(self):
        self.pose = np.zeros((3, 1))
        self.weight = 1.0 / N
        

        self.history = np.empty((0, 3))
        
        self.landmarks = np.empty((0, 5))
        self.next_landmark_id = 0
       

       
    '''compute the expected measurement for a landmark
        and the Jacobian with respect to the landmark
    '''
    def measurement_model(self, landmark_id):
        
        #two 2D vector for the position (x,y) of the observed landmark
        landmarkPos = self.landmarks[landmark_id][1]
        
        # use the current state of the particle to predict the measurment
        landmarkX = landmarkPos[0]
        landmarkY = landmarkPos[1]
        
        expectedRange = np.sqrt((landmarkX - self.pose[0])**2 + (landmarkY - self.pose[1])**2)
        
        angle = atan2(landmarkY-self.pose[1], landmarkX-self.pose[0]) - self.pose[2]
        
        
        expectedBearing = normalize_angle(angle)
        h = np.empty((2,1))
        
        try:
            h[0] = expectedRange[0]
        except:
            h[0] = expectedRange
        
        try:
            h[1] = expectedBearing[0]
        except:
            h[1] = expectedBearing

        # Compute the Jacobian H of the measurement function h wrt the landmark location
        H = np.zeros((2,2))
        H[0,0] = ((landmarkX - self.pose[0])/h[0])[0]
        
        H[0,1] = ((landmarkY - self.pose[1])/h[0])[0]
        H[1,0] = ((self.pose[1] - landmarkY)/(h[0]**2))[0]
        H[1,1] = ((landmarkX - self.pose[0])/(h[0]**2))[0]
        
        return h,H
   
       
   
    def correction_step(self, ranges, bearings, types):

        #Construct the sensor noise matrix Q_t
        Q_t = np.array([[0.1, 0.], [0., 0.1]])

        num_measurements = len(ranges)

        # Loop over each measurement
        for i in range(num_measurements):
            
            num_features = len(self.landmarks)

            w_j = np.empty((0, 1))

            # Loop over each already measured object for particle to get mesurement likelihoods
            for j in range(num_features):
                
                # Get the expected measurement and Jacobian
                [expectedZ_j, H_j] = self.measurement_model(j)

                # Landmark pose t-1
                mu_j = self.landmarks[j][1]

                # Landmark pose covariance t-1
                sigma_j = self.landmarks[j][2]
        
                # Calculate the Kalman gain
                Q_j = np.add((matmul(matmul(H_j, sigma_j), transpose(H_j))), Q_t)

                # Compute the error between the z and expectedZ
                deltaZ_j = np.array([(ranges[i] - expectedZ_j[0]), normalize_angle( bearings[i] - expectedZ_j[1] )])

                w_j = np.append(w_j, (det( (2. * pi)  * Q_j ) ** (-0.5)) * exp( (-0.5) * matmul(matmul(transpose(deltaZ_j), inverse(Q_j)), deltaZ_j)))
            
            # importance factor to new feature
            w_j = np.append(w_j, 1.0/N + 0.001 * N )

            #Set weight of this measurement
            self.weight *= np.amax(w_j)

            # Maximum likelihood index
            ML_index = np.unravel_index(np.argmax(w_j, axis=0), w_j.shape)[0]

            # Parameter to check if it is new feature or not, ML_index is increased to get count (starting from 1 not from 0) not index
            new_num_features = max(num_features, ML_index + 1)

            for j in range(new_num_features):
                
                # Is new feature?
                if j == ML_index == num_features:

                    # Initialize counter and other matrixes
                    
                    self.landmarks = np.append(self.landmarks, [[1, np.zeros((2,1)), np.zeros((2,2)), np.array([0, 0, 0, 0, 0]), self.next_landmark_id]], axis=0) # counter, mu , sigma, type, id

                    # Information used to detect cone type and for easier sorting afterward
                    self.next_landmark_id += 1 # add ID to each measurement, will stay with landmark even if one landmark is removed from list
                    if types[i] != 0:
                        self.landmarks[j][3][types[i]] += 1 # Increase measurement type detection except if unknow cone

                    # Initialize its position based on the measurement and the current robot pose:
                    self.landmarks[j][1][0] = self.pose[0] + ranges[i] * cos(self.pose[2] + bearings[i]) #x coordinate: dx = r*cos(alfa) = r*cos(theta-bearing)
                    self.landmarks[j][1][1] = self.pose[1] + ranges[i] * sin(self.pose[2] + bearings[i]) #y coordinate: dy = r*sin(alfa) = r*sin(theta-bearing)
                    
                    # get the Jacobian with respect to the landmark position
                    [h, H] = self.measurement_model(j)

                    # Initialize the covariance for this landmark
                    # TODO: Check if this is right method or not (another equation in FastSLAM 1.0 algorithm page 461)
                    #self.landmarks[j][2] = matmul(matmul(inverse(H), Q_t), transpose(inverse(H)))   
                    self.landmarks[j][2] = matmul(matmul(transpose(inverse(H)), Q_t), inverse(H))

                # Is observed feature?
                elif j == ML_index <= num_features:
                    
                    # Increase cone type probability and normalize it if it is known by type
                    if types[i] != 0:
                        self.landmarks[j][3][types[i]] *= 1.1 # Increase measurement type detection
                        self.landmarks[j][3] = np.divide(self.landmarks[j][3],  np.sum( self.landmarks[j][3] )) # Normalize result

                    # Get the expected measurement and Jacobian
                    [expectedZ, H] = self.measurement_model(j)
            
                    # Landmark pose t-1
                    mu = self.landmarks[j][1]

                    # Landmark pose covariance t-1
                    sigma = self.landmarks[j][2]
            
                    # Calculate the Kalman gain
                    Q = np.add((matmul(matmul(H, sigma), transpose(H))), Q_t)

                    K = matmul(matmul(sigma, transpose(H)), inverse(Q))
                    
                    # Compute the error between the z and expectedZ
                    deltaZ = np.array([(ranges[i] - expectedZ[0]), normalize_angle( bearings[i] - expectedZ[1] )])

                    # Update the mean and covariance of the EKF 
                    self.landmarks[j][1] = np.add( mu, matmul(K, deltaZ) )
                    self.landmarks[j][2] = matmul( np.subtract(eye(2), matmul(K, H) ), sigma)

                    #incerment counter
                    self.landmarks[j][0] += 1

                # TODO: Find things to eliminate very wrong readings!
                # else:
                #     # Should feature have not been seen?
                #     if self.landmarks[j][1] is inside perceptual range of x_t_k:
                #         self.landmarks[j][0] -= 1

                #         # If counter is too low, discard dubious feature
                #         if self.landmarks[j][0] < 0:
                #             self.landmarks.remove(self.landmarks[j])

        self.landmarks = sorted(self.landmarks, key=getKey)

        return self.weight


    # move_odom: 
    # Takes in Odometry ~ poses and updates the particles
    def mov_odom(self,odom,noise):
        #print transpose(self.pose)
        #append the old position
        self.history = np.append(self.history, transpose(self.pose), axis=0)
        
        # Calculate the distance and Guassian noise
        dist  = odom.translation

        # calculate delta rotation 1 and delta rotation 2
        delta_rot1  = odom.rotation1
        delta_rot2 = odom.rotation2
        
        # noise sigma for delta_rot1 
        sigma_delta_rot1 = noise[0]
        delta_rot1_noisy = delta_rot1 + random.gauss(0,sigma_delta_rot1)

        # noise sigma for translation
        sigma_translation = noise[1]
        translation_noisy = dist + random.gauss(0,sigma_translation)

        # noise sigma for delta_rot2
        sigma_delta_rot2 = noise[2]
        delta_rot2_noisy = delta_rot2 + random.gauss(0,sigma_delta_rot2)

        # Estimate of the new position of the robot
        x_new = self.pose[0]  + translation_noisy * cos(self.pose[2] + delta_rot1_noisy)
        y_new = self.pose[1]  + translation_noisy * sin(self.pose[2] + delta_rot1_noisy)
        theta_new = normalize_angle(self.pose[2] + delta_rot1_noisy + delta_rot2_noisy)       
        
        self.pose = np.array([x_new,y_new,theta_new])
        


def resample(w, p):
    # resample the set of particles.
    # A particle has a probability proportional to its weight to get
    # selected. A good option for such a resampling method is the so-called low
    # variance sampling, Probabilistic Robotics pg. 109
    
    # normalize the weight
    w = np.divide(w,  np.sum(w))

    if useNeff:
        neff = 1. / sum(np.power(w, 2.))

        if neff > (0.5 * N):
            
            return p

    #Initialize particles
    newParticles = []

    # Low variance re-sampling

    # the cummulative sum
    cs = np.cumsum(w)
    
    weightSum = cs[len(cs) - 1]

    # initialize the step and the current position on the roulette wheel
    step = weightSum / N
    position = random.uniform(0, weightSum)
    idx = 0

    # walk along the wheel to select the particles
    for i in range(N):
        position += step
        if (position > weightSum):
            position -= weightSum
            idx = 0
        
        while (position > cs[idx]):
            idx += 1
        
        newParticles.append(copy.copy(p[idx]))
        newParticles[i].weight = 1./N

    return newParticles



# Convert linear and angular speed to odometric motion with 3 parameters: rotation before translation, translation and rotation after translation
def convert_to_odometry_motion(last_pose, pose):
    # NB! At the moment there is no other parameters included than point mass movement (no wheel slip or anything else)
    #       After developing better motion model it is probably possible to calculate more accurate rot2 

    # Input parameters:
    #   - v is linear speed [m/s]
    #   - omega is angular speed [1/s]
    #   - dt is time between last uptade and new uptade, it is assumed that vehicle moves with constant v and omega for that amount of time. [s]

    # Output parameters:
    #   - rot1 is rotation angle before linear movement [rad]
    #   - rot2 is rotation angle after linear movement [rad]
    #   - trans is translation movement in meters after rot1 is done [m]

    dx = last_pose.position.x - pose.position.x
    dy = last_pose.position.y - pose.position.y

    q_l = last_pose.orientation
    euler_l = tf.transformations.euler_from_quaternion([q_l.x, q_l.y, q_l.z, q_l.w])
    last_theta = euler_l[2]

    q = pose.orientation
    euler = tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
    theta = euler[2]

    dth = last_theta - theta


    # r = v / omega
    # fi = omega * dt #angle what vehicle moved [rad]
    # alfa =   ( 2 * pi - fi) / 2 # This is angle in equilateral triangle, where one angle is fi and other two alfa-s
    # odom_motion.rotation1 = (pi / 2) - alfa
    # odom_motion.rotation2 = odom_motion.rotation1
    # odom_motion.translation = sqrt( (2 * r**2) - (4 * r * cos(fi)) )

    odom_motion = OdometryMotion

    odom_motion.rotation1 = dth / 2
    odom_motion.rotation2 = odom_motion.rotation1
    odom_motion.translation = sqrt( dx**2 + dy**2 )

    return odom_motion



# callback functions to process data from subscribed topics
def odometry_received(data):
    global last_odom_reading
    last_odom_reading = data



def measurements_received(data):
    global last_meas_reading
    last_meas_reading = data



def particle_filter(N): 

    # Initialization to read data
    global last_odom_reading
    global last_meas_reading

    last_odom_reading = None
    last_meas_reading = None

    # subscribing to odometry and measurements
    #rospy.Subscriber("odom_motion", OdometryMotion, odometry_received)
    rospy.Subscriber("odom", Odometry, odometry_received)
    rospy.Subscriber("cone_locations", ConeLocations, measurements_received)
    
    # Announcing published topics - map and pose
    pub_map = rospy.Publisher('map', Float64MultiArray, queue_size=10)
    pub_pose = rospy.Publisher('pose', PoseStamped, queue_size=10)
    pub_p_poses = rospy.Publisher('p_poses', Float64MultiArray, queue_size=10)
    pub_trajectory = rospy.Publisher('trajectory', Float64MultiArray, queue_size=10)
    

    estimated_pose = PoseStamped() # Position that will be published
    estimated_pose.header.frame_id = "base_link"
    estimated_pose.header.seq = 0
    estimated_pose.header.stamp = rospy.Time.now()

    # Poses of particles that will be published with matrix -> height is number of particles (N) and width is [x, y, theta]
    p_poses = Float64MultiArray() 
    p_poses.layout.dim.append(MultiArrayDimension())
    p_poses.layout.dim.append(MultiArrayDimension())
    p_poses.layout.dim[0].label = "height"
    p_poses.layout.dim[1].label = "width"
    p_poses.layout.dim[0].size = N
    p_poses.layout.dim[1].size = 3
    p_poses.layout.dim[0].stride = N*3
    p_poses.layout.dim[1].stride = 3
    p_poses.layout.data_offset = 0

  # Trajectory (List of poses) of max weight particle -> published with matrix -> height is nr of poses and width is [x, y, theta]
    trajectory = Float64MultiArray() 
    trajectory.layout.dim.append(MultiArrayDimension())
    trajectory.layout.dim.append(MultiArrayDimension())
    trajectory.layout.dim[0].label = "height"
    trajectory.layout.dim[1].label = "width"
    trajectory.layout.dim[1].size = 3
    trajectory.layout.dim[1].stride = 3
    trajectory.layout.data_offset = 0

  # Map (List of landmark locations) of max weight particle -> published with matrix -> height is nr of locations and width is [x, y, type, id]
    estimated_map = Float64MultiArray() # Map that will be published
    estimated_map.layout.dim.append(MultiArrayDimension())
    estimated_map.layout.dim.append(MultiArrayDimension())
    estimated_map.layout.dim[0].label = "height"
    estimated_map.layout.dim[1].label = "width"
    estimated_map.layout.dim[1].size = 4
    estimated_map.layout.dim[1].stride = 4
    estimated_map.layout.data_offset = 0

    # An object to maintain specific frequency of a control loop - 10hz
    r = rospy.Rate(10)  


    # Make particles
    p = []
    for i in range(N):
        robot_i = robot()
        p.append(robot_i)

    last_pose = Pose()


    # Infinite loop until shutdown
    while not rospy.is_shutdown():
        
        # motion update (prediction) from odometry

        if last_odom_reading is not None and last_meas_reading is not None:  # we cannot issue any prediction if we dont have motion data

            estimated_pose.header.seq += 1
            estimated_pose.header.stamp = rospy.Time.now() #Time when measurement and motion is updated


            odom_motion = convert_to_odometry_motion(last_pose, last_odom_reading.pose.pose)
            last_pose = last_odom_reading.pose.pose

            for i in range(N):
                p[i].mov_odom(odom_motion, noise_param)

            

        # Update particles from measurement

        #if last_meas_reading is not None:  # we cannot issue measurement update if we dont have measurements

            # measurement update (correction) + save particle poses to list to publish it
            w = np.empty((N, 1))
            p_poses.data = np.array([])

            for i in range(N):
                w[i] = p[i].correction_step(last_meas_reading.ranges, last_meas_reading.bearings, last_meas_reading.types)
                p_poses.data = np.append(p_poses.data, transpose(p[i].pose))
            
            # Highest weight particle id
            max_weight_id = np.unravel_index(np.argmax(w, axis=0), w.shape)[0][0]


            # Save trajectory to variable for publishing
            nr_steps = len(p[max_weight_id].history)
            trajectory.layout.dim[0].size = nr_steps
            trajectory.layout.dim[0].stride = nr_steps * 3 # [x, y, theta]
            # Change 2D array to 1D
            trajectory.data = p[max_weight_id].history.flatten()

            
            #TODO: Better implementation!
            # Save map to variable for publishing
            nr_landmarks = len(p[max_weight_id].landmarks)
            estimated_map.layout.dim[0].size = nr_landmarks
            estimated_map.layout.dim[0].stride = nr_landmarks * 4 # [x, y, type]
            # Change 2D array to 1D
            #estimated_map.data = p[max_weight_id].landmarks[:, 1:2].flatten()
            estimated_map.data = []
            for j in range(len(p[max_weight_id].landmarks)):
                estimated_map.data.append(p[max_weight_id].landmarks[j][1][0][0]) # x
                estimated_map.data.append(p[max_weight_id].landmarks[j][1][1][0]) # y 
                estimated_map.data.append(np.argmax(p[max_weight_id].landmarks[j][3])) # type
                estimated_map.data.append(np.max(p[max_weight_id].landmarks[j][4])) # id


            # TODO: Find avg. position for cones also, then I can send avg pos and avg cones location
            
            estimated_pose.pose.position.x = copy.copy(p[max_weight_id].pose[0])
            estimated_pose.pose.position.y = copy.copy(p[max_weight_id].pose[1])

            quaternion = tf.transformations.quaternion_from_euler(0, 0, copy.copy(p[max_weight_id].pose[2]))
            estimated_pose.pose.orientation.x = quaternion[0]
            estimated_pose.pose.orientation.y = quaternion[1]
            estimated_pose.pose.orientation.z = quaternion[2]
            estimated_pose.pose.orientation.w = quaternion[3]



            pub_pose.publish(estimated_pose)
            pub_map.publish(estimated_map)

            # Send visualization data later
            pub_trajectory.publish(trajectory)
            pub_p_poses.publish(p_poses)


            # resampling
            p = resample(w, p)

            # Get average pose and send it out
            # estimated_pose = get_position(p)
            # pose.x = estimated_pose[0]
            # pose.y = estimated_pose[1]
            # pose.theta = estimated_pose[2]

            last_meas_reading = None
            last_odom_reading = None

        # sleeping so, that the loop won't run faster than r's frequency
        r.sleep()

        

## Main loop Starts here
if __name__ == '__main__':

    #Initialize node
    rospy.init_node('slam')

    # Read particle number from parameter server or use default value
    N = rospy.get_param('N_particles', default_particle_number)

    particle_filter(N)

