"""
To run this code:
    1) open terminal in code folder
    2) write to terminal: python fastslam_framework.py ../data/sensor_data.dat ../data/world.dat 200
        - First argument is sensor data
        - Second argument is world data (for visualization)
        - Third argument is particle size N
"""

from math import *
import numpy as np
import random
import argparse
import scipy.stats
from collections import defaultdict
import math
import matplotlib.pyplot as plt
import cv2
import time
import copy

# Define parameters
steering_angle_threshold = 0.01

# Consider number of effective particles, to decide whether to resample or not
useNeff = True

# Show visualization
visual = True

# Input parameter for speed calculations
m_f = 92 # expected static mass in front [kg]
m_r = 119 # expected static mass in rear [kg]
m = m_f + m_r # full mass

WB = 1.53 # FEST18 Wheelbase [m]

l_f = WB * (m_r / m) # distance to front axle from CoM
l_r = WB * (m_f / m) # distance to rear axle from CoM


# Steering angle parameters

# Outside wheel turn angle in degrees
out_wheel_angles = np.array([0,    2,    4,    6,    8,    10,   12,   14,   16,   18,   20,   22])

# Offset from outside wheel to get inside wheel turn angle
out_in_offsets = np.array([0,    0.04, 0.16, 0.37, 0.67, 1.08, 1.60, 2.25, 3.05, 4.03, 5.21, 6.64])

# Inside wheel turn angle in degrees
in_wheel_angles = out_wheel_angles + out_in_offsets

# Steering ratio to convert steering wheel angle from data reading to actual left and right wheel position of vehicle
steering_ratios = np.array([6.98, 6.92, 6.77, 6.61, 6.43, 6.24, 6.03, 5.80, 5.55, 5.27, 4.96, 4.62])

# Parameter what is used to get actual steering ratio to calculate in wheel and out wheel angles in corner
out_steering_wheel_angles = np.multiply(out_wheel_angles, steering_ratios) 






# Functions


def read_world_data(filename):
    world_dict = defaultdict()
    f = open(filename)
    for line in f:
        line_s  = line.split('\n')
        
        line_spl  = line_s[0].split(' ')
        
        world_dict[float(line_spl[0])] = [float(line_spl[1]),float(line_spl[2])]      

    return world_dict



def read_sensor_data(filename):
    data_dict = defaultdict()
    id_arr =[]
    range_arr=[]
    bearing_arr=[]
    first_time = True
    timestamp=-1
    f = open(filename)
    for line in f:
        
        line_s = line.split('\n') # remove the new line character
        line_spl = line_s[0].split(' ') # split the line
        
        if (line_spl[0]=='ODOMETRY'):
            
            
            data_dict[timestamp,'odom'] = {'r1':float(line_spl[1]),'t':float(line_spl[2]),'r2':float(line_spl[3])}
            if (first_time == True): 
                first_time = False
                
            else: 
            
                data_dict[timestamp,'sensor'] = {'id':id_arr,'range':range_arr,'bearing':bearing_arr}                
                id_arr=[]
                range_arr = []
                bearing_arr = []

            timestamp = timestamp+1
           
                
            
        if(line_spl[0] == 'SENSOR'):
            
            id_arr.append(int(line_spl[1]))    
            range_arr.append(float(line_spl[2]))
            bearing_arr.append(float(line_spl[3]))
                              
            
    data_dict[timestamp-1,'sensor'] = {'id':id_arr,'range':range_arr,'bearing':bearing_arr}            
    return data_dict



''' Normalize the angle between -pi and pi'''       
def normalize_angle(angle):
    if(len(angle)>1):
        angle=angle[0]
    
    while(angle > math.pi):
        angle =  angle - 2*math.pi
    
    while (angle < - math.pi):
        angle = angle + 2*math.pi
        
        
    normed_angle = angle
    
    return normed_angle



# Translatse rpm unit to m/s. Constant includes FEST18 wheel size, constant pi and gearbox ratio.
def rpm_conversion(rpm):
    return  rpm * 0.048*2/60



# Converting steering wheel position to actual wheel angle using table, where ackermann is counted in
# At the moment returns avg of left and right wheel angle TODO: Should be reconsidered!
def steering_angle_conversion(steering_angle_input):
    steer_angle = abs(steering_angle_input)

    #Checks minimum threshold
    if steer_angle < steering_angle_threshold:
        return 0

    else:

        for i in range(len(out_steering_wheel_angles)):

            # Can't be equal to zero because of threshold, therefore first comparison won't work anyway 
            # and I can use list value [i-1] for interpolation
            if steer_angle <= out_steering_wheel_angles[i]:

                delta_steering_wheel_angle = out_steering_wheel_angles[i] - out_steering_wheel_angles[i - 1]
                delta = (steer_angle - out_steering_wheel_angles[i - 1]) / delta_steering_wheel_angle # Difference what is needs to be added [i-1] value to get interpolated values

                delta_steering_ratio = steering_ratios[i] - steering_ratios[i - 1]
                interpolated_ratio = steering_ratios[i - 1] + (delta * delta_steering_ratio) # Ratio, I am looking for to get actual angle

                out_wheel_angle = steer_angle / interpolated_ratio

                delta_out_in_offset = out_in_offsets[i] - out_in_offsets[i - 1]
                interpolated_offset = out_in_offsets[i - 1] + (delta * delta_out_in_offset) # Offset I need to use to get in wheel angle

                in_wheel_angle = out_wheel_angle + interpolated_offset

                # Vehicle turns right, negative value
                if steering_angle_input < 0:
                    return math.radians((-1) * (((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

                # Vehicle turns left, positive value
                else:
                    return math.radians((((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

        # This case steering angle is out of maximum limited steering position,
        # Program will use maximum parameters from table to convert this value
        # Normally program wont reach here!
        out_wheel_angle = steer_angle / steering_ratios[len(steering_ratios) - 1]
        in_wheel_angle = out_wheel_angle + out_in_offsets[len(out_in_offsets) - 1]

        # Vehicle turns right, negative value
        if steering_angle_input < 0:
            return math.radians((-1) * (((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))

        # Vehicle turns left, positive value
        else:
            return math.radians((((in_wheel_angle - out_wheel_angle) / 2) + out_wheel_angle))



# Convert steering angle and wheel speeds to linear and angular velocity [m/s]
# TODO: Test if tangens function is working properly and if pos/neg of omega is correct
def speeds_calculation(FR, FL, RL, RR, steering_angle):
    # NB! Check global variables to get all inputs for function!
    #       Steering angle needs to be converted using ackermann and turning ratio.

    # Input requirements 
    #   - Steering angle should be in radians. NB! It is not actual reading from sensor data. Ackermann and ratio needs to be considered (with lookup table)
    #   - Speeds FR, FL ... should be in meters per second [m/s]

    # Output requirements
    #   - Point mass linear speed is in meters per second [m/s]
    #   - Point mass angular velocity is in radians per second [1/s]


    v_f = (FR + FL) / 2 # front wheel speeds in bicycle model [m/s]
    v_r = (RR + RL) / 2 # rear wheel speeds in bicycle model [m/s]

    # Calculations: Using only rear wheel speed for omega calculation.
    # Probably should count front wheels also but rear wheels should be more safe in terms of wheel slip
    if (abs(steering_angle) > steering_angle_threshold):
        R_r = WB/tan(steering_angle)
        R = sqrt( R_r**2 + l_r**2 )
        omega = v_r / R_r
        speed = omega * r

    #Takes average of all four wheels for speed calculation
    else:
        speed = (v_f + v_r) / 2
        omega = 0

    return [speed, omega]

    # Unknown input parameters -> need specification
    # c_f = 100000 * 2 * (m_r / m) # N/rad
    # c_r = 100000 * 2 * (m_f / m) # N/rad

    # mu = 0.9 # depends on track

    # # Unknown parameters
    # v, omega, r, alfa_f, alfa_r, beeta, a_y

    # # Equations
    # v = omega * r 

    # a_y = v**2 / r

    # steering_angle = (WB / r) + abs(alfa_f - alfa_r)

    # alfa_f = beeta - (omega * l_f / v) + steering_angle # angle between steering angle and real front wheel movement vector in corner

    # alfa_r = beeta + (omega * l_f / v) # angle between rear wheel and real rear wheel movement vector in corner

    # alfa_f = a_y * ( m_f * 9.81 ) / c_f

    # alfa_r = a_y * ( m_r * 9.81 ) / c_r



# Convert linear and angular speed to odometric motion with 3 parameters: rotation before translation, translation and rotation after translation
def convert_to_odometry_motion(v, omega, dt):
    # NB! At the moment there is no other parameters included than point mass movement (no wheel slip or anything else)
    #       After developing better motion model it is probably possible to calculate more accurate rot2 

    # Input parameters:
    #   - v is linear speed [m/s]
    #   - omega is angular speed [1/s]
    #   - dt is time between last uptade and new uptade, it is assumed that vehicle moves with constant v and omega for that amount of time. [s]

    # Output parameters:
    #   - rot1 is rotation angle before linear movement [rad]
    #   - rot2 is rotation angle after linear movement [rad]
    #   - trans is translation movement in meters after rot1 is done [m]

    r = v / omega

    fi = omega * t #angle what vehicle moved [rad]

    alfa =   ( 2 * pi - fi) / 2 # This is angle in equilateral triangle, where one angle is fi and other two alfa-s

    rot1 = (pi / 2) - alfa

    rot2 = rot1

    trans = sqrt( (2 * R**2) - (4 * R * cos(fi)) )

    return [rot1, trans, rot2]



# Matrix calculations for better code readability

def transpose(x):
    return np.array(x).T

def inverse(x):
    return np.linalg.inv(x)

def matmul(a, b):
    return np.matmul(a, b)
        
def det(x):
    return np.linalg.det(x)

def eye(x):
    return np.eye(x)

# particle class
class robot():

    # init: 
    #    creates robot and initializes location/orientation 
    def __init__(self):
        self.pose = np.zeros((3, 1))
        self.weight = 1.0 / N

        self.history = np.empty((0, 3))
        
        self.landmarks = np.empty((0, 3))

        
        #for i in range(len(world_data)):
        #    self.landmarks[i+1] = [False,np.zeros((2,1)),np.zeros((2,2)), 0] # observed, mu , sigma, existence tau/weight
       

       
    '''compute the expected measurement for a landmark
        and the Jacobian with respect to the landmark
    '''
    def measurement_model(self, landmark_id):
        
        #two 2D vector for the position (x,y) of the observed landmark
        landmarkPos = self.landmarks[landmark_id][1]
        
        # use the current state of the particle to predict the measurment
        landmarkX = landmarkPos[0]
        landmarkY = landmarkPos[1]
        
        expectedRange = np.sqrt((landmarkX - self.pose[0])**2 + (landmarkY - self.pose[1])**2)
        
        angle = atan2(landmarkY-self.pose[1], landmarkX-self.pose[0]) - self.pose[2]
        
        
        expectedBearing = normalize_angle(angle)
        h = np.empty((2,1))
        
        try:
            h[0] = expectedRange[0]
        except:
            h[0] = expectedRange
        
        try:
            h[1] = expectedBearing[0]
        except:
            h[1] = expectedBearing

        # Compute the Jacobian H of the measurement function h wrt the landmark location
        H = np.zeros((2,2))
        H[0,0] = ((landmarkX - self.pose[0])/h[0])[0]
        
        H[0,1] = ((landmarkY - self.pose[1])/h[0])[0]
        H[1,0] = ((self.pose[1] - landmarkY)/(h[0]**2))[0]
        H[1,1] = ((landmarkX - self.pose[0])/(h[0]**2))[0]
        
        return h,H
   
       
   
    def correction_step(self, ranges, bearings):

        #Construct the sensor noise matrix Q_t
        Q_t = np.array([[0.1, 0.], [0., 0.1]])

        num_measurements = len(ranges)

        # Loop over each measurement
        for i in range(num_measurements):
            
            num_features = len(self.landmarks)

            w_j = np.empty((0, 1))

            # Loop over each already measured object for particle to get mesurement likelihoods
            for j in range(num_features):
                
                # Get the expected measurement and Jacobian
                [expectedZ_j, H_j] = self.measurement_model(j)

                # Landmark pose t-1
                mu_j = self.landmarks[j][1]

                # Landmark pose covariance t-1
                sigma_j = self.landmarks[j][2]
        
                # Calculate the Kalman gain
                Q_j = np.add((matmul(matmul(H_j, sigma_j), transpose(H_j))), Q_t)

                # Compute the error between the z and expectedZ
                deltaZ_j = np.array([(ranges[i] - expectedZ_j[0]), normalize_angle( bearings[i] - expectedZ_j[1] )])

                w_j = np.append(w_j, (det( (2. * pi)  * Q_j ) ** (-0.5)) * exp( (-0.5) * matmul(matmul(transpose(deltaZ_j), inverse(Q_j)), deltaZ_j)))
            
            # importance factor to new feature
            w_j = np.append(w_j, 1.0/N + 0.001 * N )

            #Set weight of this measurement
            self.weight *= np.amax(w_j)

            # Maximum likelihood index
            ML_index = np.unravel_index(np.argmax(w_j, axis=0), w_j.shape)[0]

            # Parameter to check if it is new feature or not, ML_index is increased to get count (starting from 1 not from 0) not index
            new_num_features = max(num_features, ML_index + 1)

            for j in range(new_num_features):
                
                # Is new feature?
                if j == ML_index == num_features:

                    # Initialize counter and other matrixes
                    
                    self.landmarks = np.append(self.landmarks, [[1, np.zeros((2,1)), np.zeros((2,2))]], axis=0) # counter, mu , sigma

                    # Initialize its position based on the measurement and the current robot pose:
                    self.landmarks[j][1][0] = self.pose[0] + ranges[i] * cos(self.pose[2] + bearings[i]) #x coordinate: dx = r*cos(alfa) = r*cos(theta-bearing)
                    self.landmarks[j][1][1] = self.pose[1] + ranges[i] * sin(self.pose[2] + bearings[i]) #y coordinate: dy = r*sin(alfa) = r*sin(theta-bearing)
                    
                    # get the Jacobian with respect to the landmark position
                    [h, H] = self.measurement_model(j)

                    # Initialize the covariance for this landmark
                    # TODO: Check if this is right method or not (another equation in FastSLAM 1.0 algorithm page 461)
                    #self.landmarks[j][2] = matmul(matmul(inverse(H), Q_t), transpose(inverse(H)))   
                    self.landmarks[j][2] = matmul(matmul(transpose(inverse(H)), Q_t), inverse(H))

                # Is observed feature?
                elif j == ML_index <= num_features:
                    
                    # Get the expected measurement and Jacobian
                    [expectedZ, H] = self.measurement_model(j)
            
                    # Landmark pose t-1
                    mu = self.landmarks[j][1]

                    # Landmark pose covariance t-1
                    sigma = self.landmarks[j][2]
            
                    # Calculate the Kalman gain
                    Q = np.add((matmul(matmul(H, sigma), transpose(H))), Q_t)

                    K = matmul(matmul(sigma, transpose(H)), inverse(Q))
                    
                    # Compute the error between the z and expectedZ
                    deltaZ = np.array([(ranges[i] - expectedZ[0]), normalize_angle( bearings[i] - expectedZ[1] )])

                    # Update the mean and covariance of the EKF 
                    self.landmarks[j][1] = np.add( mu, matmul(K, deltaZ) )
                    self.landmarks[j][2] = matmul( np.subtract(eye(2), matmul(K, H) ), sigma)

                    #incerment counter
                    self.landmarks[j][0] += 1

                # TODO: Find things to eliminate very wrong readings!
                # else:
                #     # Should feature have not been seen?
                #     if self.landmarks[j][1] is inside perceptual range of x_t_k:
                #         self.landmarks[j][0] -= 1

                #         # If counter is too low, discard dubious feature
                #         if self.landmarks[j][0] < 0:
                #             self.landmarks.remove(self.landmarks[j])

        return self.weight


    # move_odom: 
    # Takes in Odometry ~ poses and updates the particles
    def mov_odom(self,odom,noise):
        #print transpose(self.pose)
        #append the old position
        self.history = np.append(self.history, transpose(self.pose), axis=0)
        
        # Calculate the distance and Guassian noise
        dist  = odom['t']

        # calculate delta rotation 1 and delta rotation 2
        delta_rot1  = odom['r1']
        delta_rot2 = odom['r2']
        
        # noise sigma for delta_rot1 
        sigma_delta_rot1 = noise[0]
        delta_rot1_noisy = delta_rot1 + random.gauss(0,sigma_delta_rot1)

        # noise sigma for translation
        sigma_translation = noise[1]
        translation_noisy = dist + random.gauss(0,sigma_translation)

        # noise sigma for delta_rot2
        sigma_delta_rot2 = noise[2]
        delta_rot2_noisy = delta_rot2 + random.gauss(0,sigma_delta_rot2)

        # Estimate of the new position of the robot
        x_new = self.pose[0]  + translation_noisy * cos(self.pose[2] + delta_rot1_noisy)
        y_new = self.pose[1]  + translation_noisy * sin(self.pose[2] + delta_rot1_noisy)
        theta_new = normalize_angle(self.pose[2] + delta_rot1_noisy + delta_rot2_noisy)       
        
        self.pose = np.array([x_new,y_new,theta_new])
        
    


def show_measurements(p, ranges, bearings):

    for i in range(len(ranges)):
        x = p.pose[0] + ranges[i] * cos(p.pose[2] + bearings[i])
        y = p.pose[1] + ranges[i] * sin(p.pose[2] + bearings[i])
        plt.plot([p.pose[0], x], [p.pose[1], y], color='purple', linewidth=1, markersize=1)



def show_weighted_cones(landmarks, color):
    lx=[]
    ly=[]

    for i in range (len(landmarks)):
        lx.append(landmarks[i][1][0])
        ly.append(landmarks[i][1][1])

    plt.plot(lx,ly, color + 'o',markersize=10)



def show_trajectory(p):
    lx = []
    ly = [] 

    for i in range(len(p.history)):
        lx.append(p.history[i][0])
        ly.append(p.history[i][1])

    plt.plot(lx, ly, 'bo',markersize=1)



def show_position(p):
    orientation=0
    x = 0.0
    y = 0.0
    x_pos =[]
    y_pos = []

    for i in range(len(p)):
        x += p[i].pose[0]
        y += p[i].pose[1]   
        x_pos.append(p[i].pose[0])
        y_pos.append(p[i].pose[1])       
        
        orientation += p[i].pose[2]
    
    avg_orient = normalize_angle(orientation / len(p))
        
    plt.plot(x_pos,y_pos,'r.')  
    quiver_len = 3.0
    plt.quiver(x / len(p), y / len(p), quiver_len * np.cos(avg_orient), quiver_len * np.sin(avg_orient),angles='xy',scale_units='xy')





# extract position from a particle set
def get_position(p):
    x = 0.0
    y = 0.0
   
    orientation=0

    for i in range(len(p)):
        x += p[i].pose[0]
        y += p[i].pose[1]   
        
        orientation += p[i].pose[2]
    
    avg_orient = normalize_angle(orientation / len(p))
     
    return [x / len(p), y / len(p), avg_orient ]



def resample(w, p):
    # resample the set of particles.
    # A particle has a probability proportional to its weight to get
    # selected. A good option for such a resampling method is the so-called low
    # variance sampling, Probabilistic Robotics pg. 109
    
    # normalize the weight
    w = np.divide(w,  np.sum(w))

    if useNeff:
        neff = 1. / sum(np.power(w, 2.))

        if neff > (0.5 * N):
            
            return p

    #Initialize particles
    newParticles = []

    # TODO: implement the low variance re-sampling

    # the cummulative sum
    cs = np.cumsum(w)
    
    weightSum = cs[len(cs) - 1]

    # initialize the step and the current position on the roulette wheel
    step = weightSum / N
    position = random.uniform(0, weightSum)
    idx = 0

    # walk along the wheel to select the particles
    for i in range(N):
        position += step
        if (position > weightSum):
            position -= weightSum
            idx = 0
        
        while (position > cs[idx]):
            idx += 1
        
        newParticles.append(copy.copy(p[idx]))
        newParticles[i].weight = 1./N
        
    del p

    return newParticles



def visualization(t, p, w, data_dict):
    min_weight_id = np.unravel_index(np.argmin(w, axis=0), w.shape)[0][0]
    max_weight_id = np.unravel_index(np.argmax(w, axis=0), w.shape)[0][0]

    #print "Landmarks in maximum particle: ", len(p[max_weight_id].landmarks)
    #print "Landmarks in minimum particle: ", len(p[min_weight_id].landmarks)

    show_weighted_cones(p[min_weight_id].landmarks, 'y')
    show_weighted_cones(p[max_weight_id].landmarks, 'g')
    show_trajectory(p[max_weight_id])
    show_position(p)
    show_measurements(p[max_weight_id], data_dict[t,'sensor']['range'], data_dict[t,'sensor']['bearing'])    
    
    plt.plot(lx,ly,'bo',markersize=5)
    plt.axis([-2, 15, 0, 15])
    plt.draw()
    plt.pause(0.00001)
    plt.clf()


def particle_filter(data_dict,world_dict, N): # 
    # --------
    #
    # Make particles
    # 
    
    p = []
    for i in range(N):
        r = robot()
        p.append(r)

    # --------
    #
    # Update particles
    #     
    
    for t in range(len(data_dict)/2):
        start_time = time.time()
        
        # motion update (prediction)
        for i in range(N):
            p[i].mov_odom(data_dict[t,'odom'],noise_param)


        # measurement update (correction)
        w = np.empty((N, 1))
        

        for i in range(N):
            w[i] = p[i].correction_step(data_dict[t,'sensor']['range'], data_dict[t,'sensor']['bearing'])


        #Visualization
        if visual:
            visualization(t, p, w, data_dict)


        # resampling
        p = resample(w, p)

        print "Timestamp:", t, " Elapsed time:", (time.time() - start_time) * 1000, "ms"

    return get_position(p)


## Main loop Starts here
parser = argparse.ArgumentParser()
parser.add_argument('sensor_data', type=str, help='Sensor Data')
parser.add_argument('world_data', type=str, help='World Data')
parser.add_argument('N', type=int, help='Number of particles')


args = parser.parse_args()
N = args.N


noise_param = transpose([0.005, 0.01, 0.005])

if visual:
    plt.axis([0, 15, 0, 15])
    plt.ion()
    plt.show()


data_dict = read_sensor_data(args.sensor_data)
world_data = read_world_data(args.world_data)

print data_dict[0,'odom']


lx=[]
ly=[]

for i in range (len(world_data)):
    lx.append(world_data[i+1][0])
    ly.append(world_data[i+1][1])

if visual:
    plt.plot(lx,ly,'bo',markersize=3)
    plt.draw()
    plt.pause(0.0001)

estimated_position = particle_filter(data_dict,world_data,N)

if visual == True:
    time.sleep(10)

print estimated_position
