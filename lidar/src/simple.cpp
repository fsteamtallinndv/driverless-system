#include "ros/ros.h"
#include "std_msgs/String.h"
#include <bits/stdc++.h>

using namespace std;
void received(const std_msgs::String::ConstPtr& msg)
{
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
  cout << msg->data.c_str() << "\n";
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/os1_node/points", 1000, received);

  ros::spin();

  return 0;
}